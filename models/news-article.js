'use strict';

module.exports = function(sequelize, DataTypes) {
  var NewsArticle = sequelize.define('NewsArticle', {
    header: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Header must not be empty',
        },
      },
    },
    author: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    content: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Content must not be empty',
        },
      },
    }
  });
  NewsArticle.upsertWithReturn = function (options) {
    // https://github.com/sequelize/sequelize/issues/3354
    return this.findOrCreate(options).spread(function (row, created) {
      if (created) {
        return [row, created];
      } else {
        return row.updateAttributes(options.defaults).then(function (updated) {
          return [updated, created];
        });
      }
    });
  };
  return NewsArticle;
};