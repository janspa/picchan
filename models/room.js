'use strict';

module.exports = function(sequelize, DataTypes) {
  var Room = sequelize.define('Room', {
    name: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    topic: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    chanmode: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '',
    }
  });

  Room.associate = function(models) {
    Room.belongsTo(models.User, {
      as: 'owner',
    });
    Room.belongsToMany(models.User, {
      as: 'Mods',
      through: 'RoomMods',
      timestamps: false,
    });
  };

  return Room;
};