'use strict';
var bcrypt = require('bcrypt-nodejs');
var crypto = require('crypto');

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        msg: 'Username is already in use'
      },
      validate: {
        isAlphanumeric: {
          msg: 'Only letters and numbers allowed in the username',
        },
        notEmpty: {
          msg: 'Username must not be empty',
        },
        len: {
          args: [3, 40],
          msg: 'Username length must be between 3 - 40 characters',
        },
      },
    },
    password_hash: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
      },
    },
    password: {
      type: DataTypes.VIRTUAL,
      validate: {
        notEmpty: {
          msg: 'Password must not be empty',
        },
        len: {
          args: [6, Infinity],
          msg: 'Password length must be at least 6 characters',
        },
      },
    },
    password_confirm: {
      type: DataTypes.VIRTUAL,
    },
    avatar_url: {
      type: DataTypes.VIRTUAL,
      get: function () {
        var val = this.getDataValue('avatar');
        if (val === 'gravatar' && this.email) {
          var md5sum = crypto.createHash('md5');
          var hash = md5sum.update(this.email).digest('hex');
          return 'https://www.gravatar.com/avatar/'+hash.toLowerCase()+'.jpg?d=identicon&s=150';
        }
        if (val === 'picarto' && this.picarto) {
          return 'https://picarto.tv/user_data/usrimg/'+this.picarto.toLowerCase()+'/dsdefault.jpg';
        }
        return '/images/default_avatar.jpg';
      }
    },
    avatar: {
      type: DataTypes.STRING,
    },
    color: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '#b6b4d4',
      validate: {
        is: {
          args: /#[a-z0-9]{6}$/i,
          msg: 'Color must be a valid 6-digit hex value including the hash symbol (e.g. #00aa55)',
        },
      },
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique:  {
        msg: 'Email is already in use'
      },
      validate: {
        len: {
          args: [5, 256],
          msg: 'Email length must be between 5 - 256 characters',
        },
        isEmail: {
          msg: "Email address must be valid"
        }
      }
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    admin: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    picarto: {
      type: DataTypes.STRING,
      unique: {
        msg: 'Picarto username has already been connected to someone else'
      },
    },
    piczel: {
      type: DataTypes.STRING,
      unique: {
        msg: 'Piczel username has already been connected to someone else'
      },
    },
    twitch: {
      type: DataTypes.STRING,
      unique: {
        msg: 'Twitch username has already been connected to someone else'
      },
    },
    twitter: {
      type: DataTypes.STRING,
      unique: {
        msg: 'Twitter username has already been connected to someone else'
      },
    },
  },
  {
    indexes: [
      { name: 'unique_name',
        unique: true,
        fields: [{attribute: 'name', collate: 'NOCASE'}],
        msg: 'Not a unique name index',
      },
      { name: 'unique_email',
        unique: true,
        fields: [{attribute: 'email', collate: 'NOCASE'}],
        msg: 'Not a unique email index',
      },
      { name: 'unique_picarto',
        unique: true,
        fields: [{attribute: 'picarto', collate: 'NOCASE'}],
        msg: 'Not a unique picarto index',
      },
      { name: 'unique_piczel',
        unique: true,
        fields: [{attribute: 'piczel', collate: 'NOCASE'}],
        msg: 'Not a unique piczel index',
      },
      { name: 'unique_twitch',
        unique: true,
        fields: [{attribute: 'twitch', collate: 'NOCASE'}],
        msg: 'Not a unique twitch index',
      },
      { name: 'unique_twitter',
        unique: true,
        fields: [{attribute: 'twitter', collate: 'NOCASE'}],
        msg: 'Not a unique twitter index',
      },
    ],
  });

  User.associate = function(models) {
    User.belongsToMany(models.Room, {
      as: 'Mods',
      through: 'RoomMods',
      timestamps: false,
    });
    User.belongsToMany(models.Room, {
      as: 'Favorites',
      through: 'RoomFavorites',
      timestamps: false,
    });
  };
  User.prototype.verifyPassword = function(pw, done) {
    bcrypt.compare(pw, this.password_hash, done);
    // done = (err, isMatch)
  };

  var beforeSave = function(instance, options) {
    instance.email = instance.email.toLowerCase();
    if (instance.password) {
      if (instance.password !== instance.password_confirm) {
        throw new Error('Password and password confirmation must match');
      } else {
        // bcrypt.hash(data, salt, progress, callback)
        // if !salt, generates w/ default 10 rounds; otherwise uses it as-is
        /*return bcrypt.hash(instance.get('password'), null, null, function (err, hash) {
          if (err) return done(err);
          instance.set('password_hash', hash);
          return done();
        });*/
        var promise = sequelize.Promise.promisify(bcrypt.hash);
        return promise(instance.get('password'), null, null)
          .then(function (hash) {
            instance.password_hash = hash;
          });
      }
    }
  };
  User
    .beforeCreate(beforeSave)
    .beforeUpdate(beforeSave);

  return User;
};