'use strict';

var crypto = require('crypto');

module.exports = function(sequelize, DataTypes) {
  var VerificationToken = sequelize.define('VerificationToken', {
    token: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    expires: {
      type: DataTypes.DATE,
      allowNull: false,
    }
  },
  {
    hooks: {
      beforeCreate: function(instance, options, done) {
        // expires after 24 hours
        instance.expires = new Date(new Date().getTime() + (24 * 60 * 60 * 1000));
        crypto.randomBytes(32, function (err, buf) {
          if (err) return done(err);
          instance.token = buf.toString('base64').replace(/\+/g, '-').replace(/\//g, '_').replace(/\=/g, '');
          return done();
        })
      }
    }
  });

  VerificationToken.associate = function(models) {
    VerificationToken.belongsTo(models.User, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
  };
  return VerificationToken;
};