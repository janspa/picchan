const copyfiles = require('copyfiles')
const Promise = require('bluebird')

function copy(files) {
  return Promise.promisify(copyfiles)(files, {up:true})
}

const files = [[
    "./node_modules/jquery/dist/jquery.min.js",
    "./node_modules/tether/dist/js/tether.min.js",
    // "./node_modules/popper.js/dist/popper.min.js",
    "./node_modules/bootstrap/dist/js/bootstrap.min.js",
    "./node_modules/bootstrap/dist/js/bootstrap.min.js.map",
    "./node_modules/dustjs-linkedin/dist/dust-core.min.js",
    "./node_modules/dustjs-helpers/dist/dust-helpers.min.js",
    "./node_modules/wolfy87-eventemitter/EventEmitter.min.js",
    "./node_modules/socket.io-client/dist/socket.io.js",
    "./node_modules/socket.io-client/dist/socket.io.js.map",
    "./node_modules/video.js/dist/video.min.js",
    "./node_modules/videojs-contrib-hls/dist/videojs-contrib-hls.min.js",
    "./node_modules/moment/min/moment.min.js",
    // "./client/popper.min.js",
    // "./client/popper.min.js.map",
    // "./client/jquery-ui.min.js",
    // "./client/jquery.trackpad-scroll-emulator.min.js",

    "./build/js/lib"
  ],[
    "./node_modules/bootstrap/dist/css/bootstrap.min.css",
    "./node_modules/bootstrap/dist/css/bootstrap.min.css.map",
    "./node_modules/font-awesome/css/font-awesome.min.css",
    "./node_modules/video.js/dist/video-js.min.css",
    // "./client/jquery-ui.min.css",
    // "./client/trackpad-scroll-emulator.css",

    "./build/css/lib"
  ],[
    "./node_modules/font-awesome/fonts/*",

    "./build/css/fonts"
  ]
]
Promise.all(files.map(copy))
  .then(() => {
    console.log('Vendor files copied.')
  })