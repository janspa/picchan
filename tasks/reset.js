'use strict';

var arg = process.argv[2] || 'production';
var container = require('kontainer-di');
var config = require('../config/'+arg);

container.register('dbConfig', [], config.database);
require('../container-config-database')(container);
var db = container.get('databaseSetup');

module.exports = db.clear();