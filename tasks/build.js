const fs = require('fs')
const config = require('../config')
const Promise = require('bluebird')
const Database = require('../modules/database')

const db = new Database(config)

Promise.resolve().then(() => {
  if (config.database.sequelize.dialect === 'sqlite') {
    console.log(`Ensuring SQLite database file exists: ${config.database.sequelize.storage}`)
    return Promise.promisify(fs.appendFile)(config.database.sequelize.storage, '')
      .then(() => { console.log('OK') })
  }
}).then(() => {
  if (!config.database.adminDefaults.name) {
    console.log('No admin account name specified, ignoring')
  } else {
    console.log("Creating admin account")
    return db.createAdmin()
  }
}).then(() => {
  const mkdir = Promise.promisify(fs.mkdir)
  const dirs = ['build',
                'build/js',
                'build/js/lib',
                'build/css',
                'build/css/lib',
                'build/fonts']
  return Promise.all(dirs.map((d) => mkdir(d)))
    .catch((err) => {
      return null
    })
}).catch((err) => {
  console.error('Failed')
  console.error(err)
})
