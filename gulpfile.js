// var fs = require('fs');
var gulp = require('gulp');
var sass = require('gulp-sass');
var dust = require('gulp-dust');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();
var nodemon = require('gulp-nodemon');
var exec = require('child_process').exec;

gulp.task('serve',
  ['nodemon', 'babel', 'sass', 'dust', 'browser-sync'],
  function (cb) {
  /*exec('node index', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });*/
  gulp.watch('./client/js/*.js', ['babel']);
  gulp.watch(['./client/sass/*.sass', './client/sass/*.scss'], ['sass']);
  gulp.watch('./views/client/*.dust', ['dust']);
  gulp.watch('./views/*.dust', browserSync.reload);
});

gulp.task('babel', function () {
  return gulp.src('./client/js/*.js')
    .pipe(babel({
      comments: false,
      presets: ['env'],
      minified: true,
    }))
    .pipe(gulp.dest('./static/js'));
});

gulp.task('dust', function () {
  return gulp.src('./views/client/*.dust')
    .pipe(dust())
    // .pipe(fs.createWriteStream('./static/js/output.js'));
    .pipe(concat('templates.js'))
    .pipe(gulp.dest('./static/js'));
});

gulp.task('sass', function () {
  var opts = {
    outputStyle: 'compressed',
    sourceComments: 'map'
  };
  return gulp.src(['./client/sass/*.sass', './client/sass/*.scss'])
    .pipe(sass(opts).on('error', sass.logError))
    .pipe(gulp.dest('./static/css'))
    .pipe(browserSync.stream());
});

gulp.task('browser-sync', /*['nodemon'],*/ function() {
  browserSync.init({
    proxy: "http://localhost:3011",
    port: 1337,
    ui: false,
    open: false,
    notify: false,
  });
});

gulp.task('nodemon', function (cb) {
  var called = false;
  return nodemon({
    script: 'index.js',
    // watch: ['*.js'],
    // ignore: ['.git', 'node_modules', 'bower_components', '.sass-cache', 'public'],
    ignore: ['*'],
    env: { 'NODE_ENV': 'development' }
  })
  .on('start', function () {
    if (!called) cb();
    called = true;
  })
  .on('restart', function () {
    setTimeout(function() {
      browserSync.reload({ stream: false });
    }, 500);
  });
});

gulp.task('default', ['serve']);

gulp.task('reset', function (cb) {
  exec('node ./tasks/reset', function (err, stdout, stderr) {
    console.log(stdout);
    console.error(stderr);
    cb(err);
  })
});