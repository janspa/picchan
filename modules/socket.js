'use strict'
const SocketIO = require('socket.io')

const SocketModule = function (server) {
  const io = SocketIO.listen(server.server)
  // io.set('transports', [ 'polling', 'websocket' ])
  return io
}

module.exports = SocketModule
