'use strict'
const Sequelize = require('sequelize')

class DatabaseModule  {
  constructor (config) {
    this.database = new Sequelize(config.database.sequelize)
    require('../models/')(this.database)
    this.models = this.database.models
    this.Sequelize = Sequelize
    this.config = config.database
  }

  start () {
    return this.database.sync()
      .then(() => {
        console.log("[Database] Synced")
      })
  }

  createAdmin () {
    return this.database.sync()
      .return(this.database.models.User.findOrCreate({
        where: { name: this.config.adminDefaults.name },
        defaults: this.config.adminDefaults
      }))
      .spread((user, created) => {
        if (created) {
          console.log("[Database] Admin account created")
        } else {
          console.log("[Database] Admin account already exists")
        }
      })
  }

  clear () {
    return this.database.sync({force:true})
      .then(() => {
        console.log("[Database] Cleared")
      })
  }

  stop () {
    console.log("[Database] Stopping...")
    return this.database.close()
      .then(() => {
        console.log("[Database] Closed")
      })
  }

  whereNocase (column, comparator, condition) {
    if (!condition) {
      condition = comparator
      comparator = '='
    }
    return Sequelize.where(
      Sequelize.fn('lower', Sequelize.col(column)),
      comparator,
      Sequelize.fn('lower', condition))
  }
}

module.exports = DatabaseModule
