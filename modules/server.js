'use strict'
const http = require('http')
const path = require('path')
const express = require('express')
const adaro = require('adaro')
const flash = require('flash')
const cookieSession = require('cookie-session')
const moment = require('moment')
const Promise = require('bluebird')

const ServerModule = function (config) {
  const app = express()
  const server = http.createServer(app)

  const dustOptions = {
    cache: (app.get('env') === 'production'),
    helpers: [
      'dustjs-helpers',
      // 'common-dustjs-helpers',
      function (dust) {
        dust.helpers.downcase = (chunk, context, bodies, params) => {
          return chunk.capture(bodies.block, context, (data, chunk) => {
            chunk.write(data.toLowerCase())
            chunk.end()
          })
        }
        dust.helpers.css = (chunk, context, bodies, params) => {
          chunk.write('<link href="')
          chunk.render(bodies.block, context)
          chunk.write('.css" rel="stylesheet">')
          return chunk
        }
        dust.helpers.js = (chunk, context, bodies, params) => {
          chunk.write('<script src="')
          chunk.render(bodies.block, context)
          chunk.write('.js"></script>')
          return chunk
        }
        // https://github.com/lmarkus/Kraken_Example_Date_Format_Helper
        dust.helpers.formatDate = (chunk, context, bodies, params) => {
          const date = dust.helpers.tap(params.date, chunk, context)
          const format = dust.helpers.tap(params.format, chunk, context)
          const m = moment(new Date(date))
          const output = m.format(format)
          return chunk.write(output)
        }
      },
    ],
  }

  app.engine('dust', adaro.dust(dustOptions))
  app.set('view engine', 'dust')
  app.set('views', path.join(__dirname, '..', 'views', 'server'))

  app.set('trust proxy', 1)

  app.use(express.static(path.join(__dirname, '..', 'build')))
  app.use(express.static(path.join(__dirname, '..', 'static')))
  app.use(express.static(path.join(__dirname, '..', 'favicons')))

  app.use(cookieSession({
    name: 'session',
    secret: config.server.secret,
    maxAge: 31536000000 // 1 year
  }))
  app.use(flash())

  const connections = new Map()
  server.on('connection', (conn) => {
    const key = conn.remoteAddress + ':' + conn.remotePort
    connections.set(key, conn)
    conn.on('close', () => {
      connections.delete(key)
    })
  })

  function start () {
    const listenAsync = Promise.promisify(server.listen, {context: server})
    return listenAsync(config.server.port, config.server.address)
      .then(() => {
        const s = server.address()
        console.log(`[Express] Ready on ${s.address}:${s.port}`)
        return Promise.resolve()
      })
  }

  function stop () {
    console.log('[Express] Stopping...')
    server.close((err) => {
      if (err) return console.error('[Express]', err)
      console.log('[Express] Closed')
    })
    for (let conn of connections.values()) {
      conn.destroy()
    }
  }

  return {
    start,
    stop,
    app,
    server,
  }
}

module.exports = ServerModule
