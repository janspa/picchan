'use strict'
const ChatManager = require('../lib/chat/manager')

class ChatModule  {
  constructor (io, db, config) {
    this._io = io
    this.manager = new ChatManager(io, db, config.server.secret)
  }

  start () {
    this._io.use((socket, next) => {
      this.manager.verifyToken(socket.handshake.query.token)
        .then((decoded) => {
          socket.token = decoded
          next()
        })
        .catch((err) => {
          next(new Error('Invalid token'))
        })
    })
    this._io.on('connection', (socket) => {
      this.manager.createClient(socket)
        .then((client) => {
          return this.manager.createRoom(socket.token.room)
            .then((room) => {
              return room.onConnection(client, socket)
            })
        })
        .catch((err) => {
          console.error('[Chat] Error:', err)
        })
    })
    console.log('[Chat] Socket connections set up')
  }

  stop () {
    console.log('[Chat] Stopping...')
    this.manager.destroy()
    console.log('[Chat] Closed')
  }
}

module.exports = ChatModule
