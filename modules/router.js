'use strict'
const httpError = require('http-errors')
const fs = require("fs")
const path = require("path")
const Op = require("sequelize").Op

const RouterModule = function (express, db, chat) {
  const app = express.app

  // session handling
  app.use((req, res, next) => {
    if (req.method === 'GET') {
      req.session.flash = []
    }
    // set user model to res.locals if logged in
    if (req.session.userId) {
      db.models.User.findOne({
        where: { id: {[Op.eq]:req.session.userId} },
        // include: ['Favorites'],
      }).then((user) => {
        if (!user) {
          console.warn("User logged in but model not found for some reason, logging out")
          req.session.userId = null
        } else {
          res.locals.user = user
        }
        next()
        return null // necessary because of sequelize promises
      }).catch((err) => {
        next(err)
        return null
      })
    } else {
      next()
    }
  })

  // use routes
  const routeDir = path.join(__dirname, '..', 'routes')
  // index route
  app.use('/', require(path.join(routeDir, 'index'))(app, db, chat))
  // other routes
  fs.readdirSync(routeDir)
    .filter((file) => {
      return (file.indexOf(".") !== 0) && (file !== "index.js")
    })
    .forEach((file) => {
      const route = file.substr(0, file.lastIndexOf('.js'))
      app.use(`/${route}`, require(path.join(routeDir, file))(app, db, chat))
    })

  // error handling
  app.use((req, res, next) => {
    next(new httpError.NotFound())
  })
  app.use((err, req, res, next) => {
    err.status = err.status || 500
    if (app.get('env') === 'development') {
      if (err.status >= 500) {
        console.error("[Error %d] %s\n%s", err.status, req.originalUrl, err.stack)
      } else {
        console.error("[Error %d] %s", err.status, req.originalUrl)
      }
    }
    res.status(err.status).render('error', {
      error: err,
      bodyClasses: 'page-error',
      noScripts: true,
    })
  })

  return {}
}

module.exports = RouterModule
