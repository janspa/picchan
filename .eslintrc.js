module.exports = {
  "extends": "standard",
  "rules": {
    "no-var": 1,
    "comma-dangle": 0,
    "no-multi-spaces": 0,
    "key-spacing": 0,
    "quotes": 0,
  }
}