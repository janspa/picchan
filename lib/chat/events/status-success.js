'use strict'

const StatusEvent = require('./status-event')

class StatusSuccess extends StatusEvent {
  get type() {
    return "statusSuccess"
  }
}

module.exports = StatusSuccess
