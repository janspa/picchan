'use strict'

const ChatEvent = require('./chat-event')

class StatusEvent extends ChatEvent {
  static send (target, msg) {
    const instance = new this(target, msg)
    instance.send()
    return instance
  }
}

module.exports = StatusEvent
