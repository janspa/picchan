'use strict'

const StatusEvent = require('./status-event')

class StatusError extends StatusEvent {
  get type () {
    return "statusError"
  }
}

module.exports = StatusError
