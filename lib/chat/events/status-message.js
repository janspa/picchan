'use strict'

const StatusEvent = require('./status-event')

class StatusMessage extends StatusEvent {
  get type() {
    return "statusMsg"
  }
}

module.exports = StatusMessage
