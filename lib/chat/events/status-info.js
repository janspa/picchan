'use strict'

const StatusEvent = require('./status-event')

class StatusInfo extends StatusEvent {
  get type() {
    return "statusInfo"
  }
}

module.exports = StatusInfo
