'use strict'

const ChatEvent = require('./chat-event')
const request = require('request')
const Promise = require('bluebird')

class ChatMessage extends ChatEvent {
  constructor (target, client, msg) {
    super(target, msg)
    if (this.constructor === ChatMessage) {
      throw new TypeError("Cannot construct abstract ChatMessage.")
    }
    this.id = +ChatMessage.incrementalID
    this.user = { name: client ? client.name : '' }
    this.links = null
  }

  get data () {
    return Object.assign(super.data, {
      id: this.id,
      user: this.user.name || this.user,
      links: this.links,
    })
  }

  setUser (client, room) {
    this.user = {
      name: client.name,
      color: client.color,
      avatar: client.avatar,
      mode: client.chatMode(room)
    }
  }

  parseLinks () {
    const regex = /https?:\/\/[\w_-]+\.[\w_-]+.*?(?=\s|$)/gi
    const matches = this.msg.match(regex)
    if (matches && matches.length > 0) {
      this.links = []
      for (let match of matches) {
        const link = {
          url: match,
          mime: null
        }
        this.links.push(link)
      }
    }
  }

  fetchLinkMimes () {
    if (!this.links) {
      return Promise.resolve()
    }
    const fetch = []
    for (let link of this.links) {
      const requestAsync = Promise.promisify(request)
      fetch.push(requestAsync(link.url, { method: 'HEAD' }).
       then((res, body) => {
          if (res.headers && res.headers['content-type']) {
            link.mime = res.headers['content-type']
            delete link.url
          }
        })
       .catch((e) => {
          console.error(e)
       })
      )
    }
    return Promise.all(fetch)
  }

  sendLinks (newTarget) {
    const target = newTarget || this.target
    if (!target) {
      throw new TypeError("No target to send to.")
    }
    target.emit("msgLinks", {
      id: this.id,
      links: this.links
    })
  }

  fetchSendLinks (newTarget) {
    if (!this.links) {
      this.parseLinks()
    }
    if (this.links) {
      return this.fetchLinkMimes().then(() => { this.sendLinks(newTarget) })
    }
    return Promise.resolve()
  }

  static send (target, client, msg) {
    const instance = new this(target, client, msg)
    instance.parseLinks()
    instance.fetchLinkMimes().then(() => { instance.send() })
    return instance
  }
}

function AutoIncrement (n = Date.now()) {
  this.n = n
}
AutoIncrement.prototype.valueOf = function () { return this.n++ }

ChatMessage.incrementalID = new AutoIncrement()

module.exports = ChatMessage
