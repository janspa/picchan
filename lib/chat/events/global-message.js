'use strict'

const ChatMessage = require('./chat-message')

class GlobalMessage extends ChatMessage {
  constructor(target, client, msg) {
    super(target, client, msg)
  }

  get type() {
    return "msgGlobal"
  }
}

module.exports = GlobalMessage
