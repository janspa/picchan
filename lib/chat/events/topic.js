'use strict'

const ChatMessage = require('./chat-message')

class Topic extends ChatMessage {
  constructor (target, client, msg) {
    super(target, client, msg)
    if (client) this.user = client.name
  }

  get type () {
    return "topic"
  }

  static fromModelData ({user = '', msg = '', time = 0, links}) {
    const instance = new Topic(null, null, msg)
    instance.user = user
    instance.time = time
    instance.links = links
    return instance
  }
}

module.exports = Topic
