'use strict'

class ChatEvent {
  constructor (target, msg = '') {
    if (this.constructor === ChatEvent) {
      throw new TypeError("Cannot construct abstract ChatEvent.")
    }
    // if (this.type === ChatEvent.prototype.type) {
    //   throw new TypeError("Implement abstract getter `type`.")
    // }
    this.target = target
    this.msg = msg
    this.time = Date.now()
    this.sent = false
  }

  get type () {
    throw new TypeError("Do not call abstract method 'type' from child.")
  }

  get data () {
    return {
      msg: this.msg,
      time: this.time
    }
  }

  send (newTarget) {
    if (!this.type) {
      throw new TypeError("No event type specified.")
    }
    const target = newTarget || this.target
    if (!target) {
      throw new TypeError("No target to send to.")
    }
    target.emit(this.type, this.data)
    this.sent = true
  }
}

module.exports = ChatEvent
