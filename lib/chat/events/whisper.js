'use strict'

const ChatMessage = require('./chat-message')

class Whisper extends ChatMessage {
  get type () {
    return "msgWhisper"
  }
}

module.exports = Whisper
