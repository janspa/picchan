'use strict'

const ChatMessage = require('./chat-message')

class RoomMessage extends ChatMessage {
  get data () {
    if (this.sent) {
      return Object.assign(super.data, {
        user: this.user
      })
    }
    return super.data
  }

  get type () {
    if (this.sent) {
      return "msgHistory"
    }
    return "msgRoom"
  }
}

module.exports = RoomMessage
