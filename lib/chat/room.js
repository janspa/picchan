'use strict'

const Promise = require('bluebird')
const EventEmitter = require('events').EventEmitter
const handlers = require('./handlers')
const Topic = require('./events/topic')
const StatusMessage = require('./events/status-message')
const StatusError = require('./events/status-error')
const StatusInfo = require('./events/status-info')
const StatusSuccess = require('./events/status-success')

class Chatroom extends EventEmitter {
  constructor (name, io, clients) {
    super()
    // basic stuff
    this.name = name
    this.io = io
    this.clients = clients
    this.userlist = []
    this.mods = []
    this.topic = null
    this.chanmode = Chatroom.stringToChanmode('')
    this.firstUser = null
    this.msgHistory = []
    // options
    this.fetchMime = true
    this.topicLengthMax = 256
    this.msgLengthMax = 512
    this.msgHistoryMax = 10  // 0 to disable message history
  }

  get channel () {
    return this.io.to(this.name)
  }

  getClient (name) {
    // console.log(this.clients, name)
    return this.clients.get(name)
  }
  hasUser (name) {
    for (let user of this.userlist) {
      if (user === name) {
        return true
      }
    }
    return false
  }
  addUser (name) {
    this.userlist.push(name)
  }
  removeUser (name) {
    for (let [i, user] of this.userlist.entries()) {
      if (user === name) {
        return this.userlist.splice(i, 1)
      }
    }
  }
  accessLevel (client) {
    if (!client.user) {
      return Chatroom.access.GUEST
    } else if (client.user.admin) {
      return Chatroom.access.ADMIN
    } else if (this.model && this.model.ownerId === client.user.id) {
      return Chatroom.access.OWNER
    } else if (this.mods.find((mod) => { return mod.id === client.user.id })) {
      return Chatroom.access.MOD
    } else if (client.isLoggedIn) {
      return Chatroom.access.USER
    }
    return Chatroom.access.GUEST
  }
  isUserMod (client) {
    return this.accessLevel(client) === Chatroom.access.MOD
  }
  isUserOwner (client) {
    return this.accessLevel(client) === Chatroom.access.OWNER
  }
  isUserAdmin (client) {
    return this.accessLevel(client) === Chatroom.access.ADMIN
  }

  setModel (model) {
    if (!model) {
      return Promise.reject(new Error("Tried to set a null model"))
    } else {
      this.model = model
      try {
        this.topic = Topic.fromModelData(JSON.parse(model.topic))
      } catch (e) {
        // this.topic = Topic.fromModelData({})
      }
      this.chanmode = Chatroom.stringToChanmode(model.chanmode)
      return model.getMods()
        .then((mods) => {
          this.mods = mods
          return this
        })
    }
  }
  unsetModel () {
    this.model = null
    this.chanmode = {}
    this.mods = []
    this.sendChanmode()
    this.sendUsers()
    this.sendInfo("This room has been unregistered.")
  }

  onConnection (client, socket) {
    // kick unallowed guest and stop
    if (this.chanmode.regOnly && !client.isLoggedIn) {
      this.sendError("Guests aren't allowed to join in this room.", socket)
      this.emit('kick', client, socket)
      // if the room has no more users, emit empty
      if (this.userlist.length === 0) {
        this.emit('empty')
      }
      // peace out
      return
    }
    // otherwise join in the room
    socket.join(socket.token.room)
    // handle the events
    for (let i in handlers) {
      socket.on(i, (data) => handlers[i].call(this, client, data, socket))
    }
    // if the room doesn't already have a user by this name, add them
    if (!this.hasUser(client.name)) {
      this.addUser(client.name)
      // also broadcast join to everyone in the channel except the joining socket
      socket.broadcast.to(this.name).emit('join', client.userData(this))
    }
    // say hi
    this.sendInitial(socket, client)

    // if the room hasn't been registered yet
    if (!this.model && client.isLoggedIn) {
      if (!this.firstUser) {
        // reserve owner rights for this user
        this.firstUser = client.user.id
      }
      if (this.firstUser === client.user.id) {
        this.sendInfo("This room hasn't been registered yet. If you want to register the room, type <code>/register</code>. For more information, check the <a href='/help'>help page</a>.", socket)
      }
    }
  }

  addToHistory (msg, client) {
    if (this.msgHistoryMax > 0) {
      while (this.msgHistory.length >= this.msgHistoryMax) {
        this.msgHistory.shift()
      }
      msg.setUser(client, this)
      this.msgHistory.push(msg)
    }
  }

  sendNotification (msg, socketOrClient) {
    StatusMessage.send(socketOrClient || this.channel, msg)
  }
  sendError (msg, socketOrClient) {
    StatusError.send(socketOrClient || this.channel, msg)
  }
  sendSuccess (msg, socketOrClient) {
    StatusSuccess.send(socketOrClient || this.channel, msg)
  }
  sendInfo (msg, socketOrClient) {
    StatusInfo.send(socketOrClient || this.channel, msg)
  }

  sendInitial (socket, client) {
    if (Chatroom.chanmodeToString(this.chanmode)) {
      this.sendChanmode(socket)
    }
    this.sendUsers(socket)
    if (this.topic) {
      this.topic.send(socket)
    }
    this.sendHistory(socket)
  }

  sendEvent (event, data, socket) {
    if (socket) {
      socket.emit(event, data)
    } else {
      this.channel.emit(event, data)
    }
  }

  sendChanmode (socket) {
    this.sendEvent('chanmode', Chatroom.chanmodeToString(this.chanmode), socket)
  }
  sendHistory (socket) {
    for (let msg of this.msgHistory) {
      msg.send(socket)
    }
  }
  sendUsers (socket) {
    const data = this.userlist.map((user) => this.getClient(user).userData(this))
    this.sendEvent('users', data, socket)
  }

  destroy () {
    this.removeAllListeners()
  }

  static chanmodeToString (chanmode) {
    let modeStr = ''
    // only registered users allowed to talk
    if (chanmode.moderated) { modeStr += 'm' }
    // channel is locked with a password [not implemented]
    if (chanmode.keylock)   { modeStr += 'k' }
    // only registered users allowed to join the channel
    if (chanmode.regOnly)   { modeStr += 'r' }
    // topic may be changed by mods or higher only
    if (chanmode.topicLock) { modeStr += 't' }

    if (modeStr) { modeStr = `+${modeStr}` }
    return modeStr
  }

  static stringToChanmode (data, currentMode) {
    const mode = currentMode || {}
    const bool = data[0] === '+'
    for (let flag of data) {
      switch (flag) {
        case 'm': mode.moderated = bool; break
        case 'k': mode.keylock   = bool; break
        case 'r': mode.regOnly   = bool; break
        case 't': mode.topicLock = bool; break
      }
    }
    return mode
  }

  static get access () {
    return {
      ADMIN: 4,
      OWNER: 3,
      MOD:   2,
      USER:  1,
      GUEST: 0,
    }
  }
}

module.exports = Chatroom
