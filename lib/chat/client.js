'use strict'

const Promise = require('bluebird')

class Client {
  constructor (socket) {
    if (socket == null) {
      throw new Error('A Client needs a socket.')
    }
    this.sockets = [socket]
    this.token = socket.token
    this.id = socket.id
    this.ip = socket.handshake.headers['x-forwarded-for'] || socket.handshake.address
    this.user = null
    this.lastMsgDate = null
    this.away = false
    this.unawayOnMsg = true
    this.guestColor = '#b6b4d4'
  }

  static get FLOOD_DELAY () { return 100 }

  setUser (user) {
    this.user = user
  }

  addSocket (socket) {
    this.sockets.push(socket)
  }

  removeSocket (socket) {
    socket.disconnect(true)
    socket.removeAllListeners()
    socket.client.destroy()
    for (let [i, socketEntry] of this.sockets.entries()) {
      if (socketEntry.id === socket.id) {
        return this.sockets.splice(i, 1)
      }
    }
  }

  hasSockets (room) {
    if (!room) {
      // if there's no room specified, just return if client has any sockets
      return this.sockets.length > 0
    } else {
      // loop through the sockets and see if any of them belongs to room
      for (let socket of this.sockets) {
        if (socket.token.room === room) {
          return true
        }
      }
      return false
    }
  }

  emit (evt, data) {
    for (let socket of this.sockets) {
      socket.emit(evt, data)
    }
  }

  static seeder (s) {
    return function () {
      s = Math.sin(s) * 10000
      return s - Math.floor(s)
    }
  }

  static randomString (len, chars, random) {
    if (!random) {
      random = Math.random()
    } else if (typeof random === 'number') {
      random = Client.seeder(random)
    }
    if (!chars) {
      chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    }
    let str = ''
    for (let i = 0; i < len; ++i) {
      str += chars.charAt(Math.floor(random() * chars.length))
    }
    return str
  }

  get isLoggedIn () {
    return this.token && this.token.username && this.user
  }

  get name () {
    if (this.isLoggedIn) {
      return this.user.name
    } else if (this.guestName) {
      return this.guestName
    } else if (this.ip) {
      const seed = parseInt(this.ip.replace(/\D/g, ''))
      const str = Client.randomString(5, null, seed)
      this.guestName = `guest_${str}`
    } else {
      this.guestName = `guest_${(this.sockets[0].id).substr(0, 5)}`
    }
    return this.guestName
  }

  get color () {
    if (this.user) {
      return this.user.color
    }
    return this.guestColor
  }
  set color (color) {
    if (this.user) {
      this.user.color = color
      this.user.save()
    } else {
      this.guestColor = color
    }
  }

  get avatar () {
    if (this.user) {
      return this.user.avatar_url
    }
    return null
  }

  get connections () {
    if (this.user) {
      return {
        picarto: this.user.picarto,
        piczel: this.user.piczel,
        twitch: this.user.twitch,
        twitter: this.user.twitter,
      }
    }
    return null
  }

  get isFlooding () {
    return (this.lastMsgDate != null) && ((Date.now() - this.lastMsgDate) < Client.FLOOD_DELAY)
  }

  updateLastMsg () {
    this.lastMsgDate = Date.now()
  }

  favorite (room, setOrRemove) {
    if (!this.user) {
      return Promise.reject(new Error('User model missing.'))
    } else if (!room.model) {
      return Promise.reject(new Error('Room model missing.'))
    } else {
      if (setOrRemove) {
        return this.user.setFavorites([room.model])
          .then(favs => favs.length > 0)
      } else {
        return this.user.removeFavorites([room.model])
          .then(removed => removed > 0)
      }
    }
  }

  chatMode (room) {
    let mode = 0
    mode |= this.isLoggedIn ? 1 : 0
    if (mode > 0) {
      mode |= room.isUserMod(this)   ? 2 : 0
      mode |= room.isUserOwner(this) ? 4 : 0
      mode |= room.isUserAdmin(this) ? 8 : 0
    }
    return mode
  }

  destroy () {
    for (let socket of this.sockets) {
      socket.disconnect(true)
      socket.removeAllListeners()
      // socket.client.destroy()
    }
  }

  userData (room) {
    return {
      name: this.name,
      color: this.color,
      mode: this.chatMode(room),
      away: this.away,
      avatar: this.avatar,
      connections: this.connections,
    }
  }
}

module.exports = Client
