'use strict'

const he = require('he')
const RoomMessage = require('./events/room-message')
const GlobalMessage = require('./events/global-message')
const Whisper = require('./events/whisper')
const Topic = require('./events/topic')

function validate (data, validator) {
  if (typeof data !== typeof validator) {
    return false
  }
  if (typeof data === 'object') {
    for (let prop in validator) {
      if (!validate(data[prop], validator[prop])) {
        return false
      }
    }
  }
  return true
}
validate.string = function (data) {
  return typeof data === 'string'
}
validate.number = function (data) {
  return typeof data === 'number'
}
validate.boolean = function (data) {
  return typeof data === 'boolean'
}

// in every function `this` is assumed to be an instance of Chatroom

const HANDLERS = {
  disconnect: function (client, data, socket) {
    // remove the socket that just disconnected
    client.removeSocket(socket)
    // if that client has no more sockets in this room, remove and announce quit
    if (!client.hasSockets(this.name)) {
      this.removeUser(client.name)
      this.channel.emit('quit', client.userData(this))
    }
    this.emit('quit', client, socket)
    // if the room has no more users, emit empty
    if (this.userlist.length === 0) {
      this.emit('empty')
    }
  },

  msgRoom: function (client, data, socket) {
    if (!client.isLoggedIn && this.chanmode.moderated) {
      this.sendError("Guests aren't allowed to chat in this room.", socket)
    } else if (!client.isFlooding && validate.string(data)) {
      const escaped = he.escape(data.substring(0, this.msgLengthMax))
      const msg = new RoomMessage(this.channel, client, escaped)

      client.updateLastMsg()
      if (client.away && client.unawayOnMsg) {
        // TODO: not too good but I guess it works
        HANDLERS.away.call(this, client, false)
      }
      msg.send()
      msg.fetchSendLinks()
      this.addToHistory(msg, client)
    }
  },

  msgGlobal: function (client, data, socket) {
    if (this.accessLevel(client) >= this.constructor.access.ADMIN) {
      if (validate.string(data)) {
        const escaped = he.escape(data)
        const msg = new GlobalMessage(this.io, client, escaped)

        client.updateLastMsg()
        msg.send()
        msg.fetchSendLinks()
        this.addToHistory(msg, client)
      }
    }
  },

  msgWhisper: function (client, data, socket) {
    if (!client.isFlooding && validate(data, {to:'', msg:''})) {
      const recipient = this.getClient(data.to)
      if (recipient) {
        const escaped = he.escape(data.msg.substring(0, this.msgLengthMax))
        const msg = new Whisper(recipient, client, escaped)
        client.updateLastMsg()
        msg.send()
        msg.fetchSendLinks()
      } else {
        this.sendError(`Recipient ${data.to} not found.`, socket)
      }
    }
  },

  color: function (client, data, socket) {
    if (validate.string(data)) {
      data = data.trim()
      if (data.match(/^#[a-fA-F0-9]{6}$/)) {
        client.color = data
        Object.keys(socket.rooms).filter(id => id != socket.id).forEach((room) => {
          this.io.to(room).emit('color', {
            user: client.name,
            color: data
          })
        })
      } else {
        this.sendError("Incorrect format: must be #XXXXXX where X is 0-9 or A-F.", socket)
      }
    }
  },

  chanmode: function (client, data, socket) {
    if (this.model) {
      if (validate.string(data) &&
          data.length > 1 &&
          (data[0] === '+' || data[0] === '-')) {
        if (this.accessLevel(client) >= this.constructor.access.MOD) {
          // for now there are no owner-only modes so we can just do this
          const mode = this.constructor.stringToChanmode(data, this.chanmode)
          const str = this.constructor.chanmodeToString(mode)
          this.model.update({ chanmode: str })
            .then(() => {
              this.chanmode = mode
              this.sendChanmode()
            })
        } else {
          this.sendError("Insufficient privileges to set mode.", socket)
        }
      } else {
        this.sendError("Mode string must start with + or -.", socket)
      }
    } else {
      this.sendError("Can't set a mode to an unregistered room.", socket)
    }
  },

  away: function (client, data, socket) {
    client.away = (data != null) ? !!data : !client.away
    this.io.emit('away', { user: client.name, away: client.away })
  },

  topic: function (client, data, socket) {
    if (this.chanmode.topicLock && (this.accessLevel(client) < this.constructor.access.MOD)) {
      this.sendError("You don't have the permission to set the topic.", socket)
    } else if (validate.string(data)) {
      const escaped = he.escape(data.trim().substring(0, this.topicLengthMax))
      const topic = new Topic(this.channel, client, escaped)
      this.topic = topic
      topic.send()
      topic.fetchSendLinks(this.channel)
        .then(() => {
          if (this.model) {
            return this.model.update({ topic: JSON.stringify(topic.data) })
          }
        })
        .catch((e) => {
          this.sendError("Failed to save the topic.")
        })
    }
  },

  register: function (client, data, socket) {
    if (this.model) {
      this.sendError("This room has already been registered.", socket)
    } else if (!client.isLoggedIn ||
               this.firstUser && this.firstUser !== client.user.id) {
      // if no firstUser, the room has been unregistered but not disposed yet
      this.sendError("You don't have registration rights.", socket)
    } else {
      this.emit('register', client, socket)
    }
  },

  unregister: function (client, data, socket) {
    if (!this.model) {
      this.sendError("This room has not been registered.")
    } else if (this.accessLevel(client) < this.constructor.access.OWNER) {
      this.sendError("Only the owner can unregister a room.", socket)
    } else {
      this.emit('unregister', client, socket)
    }
  },

  favorite: function (client, data, socket) {
    if (!this.model) {
      this.sendError("Can't favorite an unregistered room.", socket)
    } else if (!client.isLoggedIn) {
      this.sendError("You need to be logged in to favorite a room.", socket)
    } else {
      const setOrRemove = !!data
      client.favorite(this, setOrRemove)
        .then((success) => {
          if (setOrRemove) {
            if (success) {
              this.sendSuccess("Room was added to your favorites.", socket)
            } else {
              this.sendError("Room is already in your favorites.", socket)
            }
          } else {
            if (success) {
              this.sendSuccess("Room was removed from your favorites.", socket)
            } else {
              this.sendError("Room wasn't in your favorites in the first place.", socket)
            }
          }
        })
        .catch((err) => {
          this.sendError(err.message, socket)
        })
    }
  },

}

module.exports = HANDLERS
