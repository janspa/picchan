'use strict'

const jwt = require('jsonwebtoken')
const Promise = require('bluebird')
const Client = require('./client')
const Chatroom = require('./room')
const Op = require('sequelize').Op

class ChatManager  {
  constructor (io, db, secret) {
    this.io = io
    this.db = db
    this.secret = secret
    this.rooms = new Map()
    this.clients = new Map()
    this.destroyEmptyRooms = true
  }

  static transformRoomName (name) {
    return name.trim().toLowerCase().substr(0, 128)
  }

  getRoom (name) {
    return this.rooms.get(ChatManager.transformRoomName(name))
  }
  addRoom (room) {
    this.rooms.set(ChatManager.transformRoomName(room.name), room)
  }
  deleteRoom (name) {
    return this.rooms.delete(ChatManager.transformRoomName(name))
  }

  getClient (name) {
    return this.clients.get(name)
  }
  addClient (client) {
    this.clients.set(client.name, client)
  }
  deleteClient (client) {
    if (typeof client === 'string') client = this.clients.get(client)
    if (!client) throw new Error(`Client not found`)
    client.destroy()
    return this.clients.delete(client.name)
  }

  createClient (socket) {
    const name = socket.token.username || null
    const isGuest = !name
    let client = this.getClient(name)
    if (isGuest) {
      // check if a guest client with the same name (IP) already exists
      const guest = new Client(socket)
      client = this.getClient(guest.name)
    }
    if (client) {
      // a client with the same user/guest found, just add the socket
      client.addSocket(socket)
      return Promise.resolve(client)
    } else {
      const newClient = new Client(socket)
      if (isGuest) {
        // add to clients with guest name
        this.addClient(newClient)
        // we done
        return Promise.resolve(newClient)
      } else {
        return this.db.models.User.findOne({where: {name: {[Op.eq]: name}}})
          .then((user) => {
            // set user instance for the new user
            newClient.setUser(user)
            // add to clients with user name
            this.addClient(newClient)
            return newClient
          })
      }
    }
  }

  createRoom (name) {
    const room = this.getRoom(name)
    if (room) {
      return Promise.resolve(room)
    } else {
      console.log(`[ChatManager] Creating room ${name}`)
      return this.db.models.Room.findOne({where: {name: {[Op.eq]: name}}})
        .then((model) => {
          const room = new Chatroom(name, this.io, this.clients)
          this.addRoom(room)
          return model ? room.setModel(model).then(() => room) : room
        })
        .then((room) => {
          this.makeRoomEventHandlers(room)
          return room
        })
    }
  }

  makeRoomEventHandlers (room) {
    room.on('quit', (client, socket) => {
      if (!client.hasSockets()) {
        this.deleteClient(client)
      }
    })
    room.on('kick', (client, socket) => {
      this.deleteClient(client)
    })
    room.on('empty', (client, socket) => {
      if (this.destroyEmptyRooms) {
        this.destroyRoom(room.name)
      }
    })
    room.on('register', (client, socket) => {
      this.saveRoom(room, client.user)
        .then((instance) => {
          return room.setModel(instance)
        })
        .then(() => {
          room.sendSuccess("Room was succesfully registered!", socket)
        })
        .catch((err) => {
          console.error(err)
          room.sendError(err.message, socket)
        })
    })
    room.on('unregister', (client, socket) => {
      console.log(`[ChatManager] Unregistering room ${room.name}`)
      return room.model.destroy()
        .then(() => {
          room.sendSuccess("Room was succesfully unregistered!", socket)
          room.unsetModel()
        })
        .catch((err) => {
          console.error(err)
          room.sendError(err.message, socket)
        })
    })
  }

  saveRoom (room, owner) {
    console.log("[ChatManager] Registering room %s to %s", room.name, owner.name)
    return this.db.models.Room.findOrCreate({
      where: {
        name: room.name,
      },
      defaults: {
        topic: room.topic,
      },
    })
      .spread((instance, created) => {
        if (!created) {
          return Promise.reject(new Error('Room has already been registered.'))
        } else {
          return instance.setOwner(owner).then(() => instance)
        }
      })
  }

  destroyRoom (name) {
    const room = this.getRoom(name)
    if (room) {
      room.destroy()
      this.deleteRoom(room.name)
      console.log(`[ChatManager] Destroyed room ${name}`)
    } else {
      console.warn(`[ChatManager] Tried to destroy room ${name} but it was not found`)
    }
  }

  destroy () {
    const StatusError = require('./events/status-error')
    StatusError.send(this.io, "<b>Server is shutting down.</b>")
    for (let room of this.rooms.values()) {
      room.destroy()
    }
    this.rooms.clear()
    for (let client of this.clients.values()) {
      client.destroy()
    }
    this.clients.clear()
  }

  createToken (room, username) {
    if (!username) return jwt.sign({room: room}, this.secret)
    return jwt.sign({room: room, username: username}, this.secret)
  }

  verifyToken (token, cb) {
    return Promise.promisify(jwt.verify)(token, this.secret)
  }
}

module.exports = ChatManager
