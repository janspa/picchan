const commandMap = {}
const commands = []

const ChatCommands = {
  setCommand: (cmds, description, fn, hidden) => {
    const opts = { cmds: cmds, description: description, fn: fn, hidden: !!hidden }
    if (typeof opts.cmds === 'string') {
      opts.cmds = [opts.cmds]
    }
    for (let c of opts.cmds) {
      commandMap[c] = opts
    }
    commands.push(opts)
  },

  runCommand: (chat, msg, otherwise) => {
    const split = msg.split(' ')
    const cmd = split[0].substring(1)
    if (commandMap[cmd]) {
      const args = split.slice(1)
      if (commandMap[cmd].fn(chat, args) === false) {
        ChatCommands.runCommand(chat, `/help ${cmd}`)
      }
    } else if (otherwise) {
      otherwise(cmd)
    }
  },
}

// return false to automatically display help for the command

ChatCommands.setCommand(
  'help',
  'Lists the available commands.',
  function (chat, args) {
    if (args.length > 0) {
      const c = commandMap[args[0]]
      if (c) {
        chat.statusInfo(`<b>${c.cmds.join(', ')}</b> ${c.description}`)
      }
    } else {
      const str = commands
        .filter(c => !c.hidden)
        .map(c => `<b>${c.cmds.join(', ')}</b> ${c.description}`)
        .join('<br>')
      chat.statusInfo(str)
    }
  }
)
ChatCommands.setCommand(
  ['watch', 'picarto'],
  '[streamer ...] Embeds Picarto streamers to the player panel.',
  function (chat, args) {
    if (args.length > 0) {
      args.forEach((channel) => {
        chat.loadStream('picarto', channel)
      })
    } else {
      return false
    }
  }
)
ChatCommands.setCommand(
  ['whisper', 'w'],
  '[recipient] [message] Whispers a private message.',
  function (chat, args) {
    if (args.length > 1) {
      const to = args.shift()
      const msg = args.join(' ')
      chat.sendWhisper(to, msg)
    } else {
      return false
    }
  }
)
ChatCommands.setCommand(
  'mode',
  '[+|-flags] Sets room modes. See <a href="/help#modes">help</a> for possible flags.',
  function (chat, args) {
    if (args.length > 0) {
      chat.socket.emit('chanmode', args[0])
    } else {
      return false
    }
  }
)
ChatCommands.setCommand(
  ['favorite', 'fav'],
  'Adds the room to your favorites.',
  function (chat, args) {
    chat.socket.emit('favorite', true)
  }
)
ChatCommands.setCommand(
  ['unfavorite', 'unfav'],
  'Removes the room from your favorites.',
  function (chat, args) {
    chat.socket.emit('favorite', false)
  }
)
ChatCommands.setCommand(
  'away',
  'Sets or unsets your away status.',
  function (chat, args) {
    chat.socket.emit('away')
  }
)
ChatCommands.setCommand(
  'color',
  '[#xxxxxx] Changes the username color. Valid 6-digit hex color code (including #) required.',
  function (chat, args) {
    if (args.length > 0) {
      chat.socket.emit('color', args[0])
    } else {
      return false
    }
  }
)
ChatCommands.setCommand(
  ['hilight', 'highlight'],
  '[word ...] Adds words that will be highlighted in the chat. Available wildcards: ? for single characters (e.g. "ok?" matches "ok" and "oka" but not "okay") and * for globs (e.g. "*ok*" matches "bazooka").',
  function (chat, args) {
    if (args.length > 0) {
      for (let hilight of args) {
        const oldHilights = chat.options.get('Highlighted words')
        chat.options.set('Highlighted words', (`${oldHilights} ${hilight}`).trim())
        chat.statusSuccess(`Added highlight: ${hilight}`)
      }
    } else {
      if (chat.options.hasHilights()) {
        chat.statusInfo(`Hilighted words: ${chat.options.get('Highlighted words').split(' ').join(', ')}`)
      } else {
        chat.statusInfo('No highlighted words.')
      }
    }
  }
)
ChatCommands.setCommand(
  ['unhilight', 'unhighlight'],
  '[word ...] Removes words from the highlight list.',
  function (chat, args) {
    if (args.length > 0) {
      for (let hilight of args) {
        const oldHilights = chat.options.get('Highlighted words')
        const index = oldHilights.toLowerCase().indexOf(hilight.toLowerCase())
        if (index >= 0) {
          const cutHilights = oldHilights.substr(0, index) + oldHilights.substr(index + hilight.length)
          chat.options.set('Highlighted words', cutHilights.trim())
          chat.statusSuccess(`Removed highlight: ${hilight}`)
        } else {
          chat.statusError(`${hilight} not found in the highlight list`)
        }
      }
    } else {
      return false
    }
  }
)
ChatCommands.setCommand(
  'topic',
  '[message] Sets the room topic. 128 characters max.',
  function (chat, args) {
    if (args.length > 0) {
      chat.socket.emit('topic', args.join(' '))
    } else {
      return false
    }
  }
)
ChatCommands.setCommand(
  'register',
  'Registers the room.',
  function (chat, args) {
    chat.socket.emit('register')
  }
)
ChatCommands.setCommand(
  'unregister',
  'Unregisters the room.',
  function (chat, args) {
    chat.socket.emit('unregister')
  }
)
ChatCommands.setCommand(
  'global',
  '[message] (Admin) Sends a global message.',
  function (chat, args) {
    chat.socket.emit('msgGlobal', args.join(' '))
  },
  true
)

export default ChatCommands
