import ChatCore from './chat-core'
import ChatOptions from './chat-options'
import ChatCommands from './chat-commands'
import ChatWindow from './chat-window'
import { Player, PicartoPlayer } from './player'

export default class ChatFront {
  constructor (container) {
    this.core = new ChatCore({
      roomName: container.data('roomName'),
      roomId: container.data('roomId')
    })

    this.el = {}
    this.el.container = container
    this.el.ul = container.find('.chat-ul')
    this.el.main = container.find('.chat-main')
    this.el.aside = container.find('.chat-aside')
    this.el.users = container.find('.chat-aside-userlist')
    this.el.topic = container.find('.chat-topic')
    this.el.chanmode = container.find('.chat-chanmode')
    this.el.userCount = container.find('.chat-user-count')
    this.el.guestCount = container.find('.chat-guest-count')
    this.el.inputForm = container.find('.chat-input-form')
    this.el.scrollable = this.el.ul.closest('.tse-scrollable')
    this.el.scrollContent = this.el.ul.closest('.tse-scroll-content')
    if (this.el.scrollContent.length === 0) {
      throw new Error("Initialize TSE before Chat")
    }

    this.myUsername = ''
    this.lastUser = ''
    this.lastTimestamp = null
    this.inputHistory = []
    this.autoScroll = true

    this.core.connect()
      .catch((err) => {
        this.core.statusError(err)
      })
      .then(() => {
        document.title = `${this.roomName} - ${document.title}`
        this.makeHandlers()
        this.makeListeners()
        for (let c in ChatFront.carvings) {
          container.append(`<audio id="audio_${c}" src="/media/${c}.mp3">`)
        }
        return new Promise((resolve, reject) => {
          window.jQuery.getJSON('/emotes.json')
            .done((data) => {
              ChatFront.emotes = new Map(Object.entries(data))
              resolve()
            })
            .fail(reject)
        })
      })
      .catch((err) => {
        this.core.statusError('Failed to load emotes.')
      })
  }

  get roomName () {
    return this.core.roomName
  }

  get isScrolledToBottom () {
    const c = this.el.scrollContent
    const margin = 10
    return c.scrollTop() + c.height() > c.prop('scrollHeight') - margin
  }
  scrollToBottom () {
    const c = this.el.scrollContent
    c.stop()
    c.animate({ scrollTop: c.prop('scrollHeight') }, 'fast')
  }
  updateAutoScroll () {
    this.autoScroll = this.isScrolledToBottom
  }
  doAutoScroll () {
    if (this.autoScroll) {
      this.scrollToBottom()
    }
  }

  writeMessage (data, template) {
    if (data.user) {
      data.ignored = ChatOptions.isIgnored(data.user.name || data.user)
      data.hilight = ChatOptions.isHilighted(data.msg)
    }
    ChatFront.parseMsgData(data)
    if (template === 'chat-msg' && this.lastTimestamp === data.timestamp) {
      data.timestamp = null
    } else if (template === 'chat-msg-container') {
      data.container = true
    }
    dust.render(template, data, (err, out) => {
      if (err) return console.error(err)
      this.updateAutoScroll()
      if (template !== 'chat-msg') {
        this.lastUser = ''
        $(out).appendTo(this.el.ul)
      } else {
        // merge to the previous container
        const last = this.el.ul.find('li:last-child').find('.chat-msg:last')
        $(out).insertAfter(last)
      }
      this.el.scrollable.TrackpadScrollEmulator('recalculate')
      this.doAutoScroll()
      this.attachLinks(data)
    })
  }

  statusEvent (data, status) {
    if (typeof data === 'string') data = { msg: data }
    if (status) data.status = status
    if (!data.time) data.time = Date.now()
    this.writeMessage(data, 'chat-statusmsg')
  }

  makeListeners () {
    this.el.inputForm.submit((e) => {
      e.preventDefault()
      const input = $(e.target.firstChild)
      const msg = input.val()
      input.val('')
      this.core.sendMessage(msg)
    })
  }

  makeHandlers () {
    // socket.io events
    this.core.socket.on('connect', () => {
      this.statusEvent(`Connected to ${this.roomName}`, 'join')
    })
    this.core.socket.on('connect_error', (error) => {
      if (this.core.socket.io.backoff.attempts <= 1) {
        console.error(error)
        this.statusEvent(`Connection error: ${error.description.message || error.message}`, 'error')
      }
    })
    this.core.socket.on('disconnect', () => {
      this.statusEvent(`Disconnected from ${this.roomName}`, 'error')
    })
    this.core.socket.on('reconnecting', (num) => {
      if (num === 1) {
        this.statusEvent(`Attempting to reconnect...`)
      } else if (num === 2) {
        this.statusEvent(`Still attempting to reconnect...`)
      }
    })
    this.core.socket.on('error', (err) => {
      this.statusEvent(err, 'error')
    })

    // chat message events
    this.core.on('msgRoom', (data) => {
      if (data.user.name === this.lastUser) {
        this.writeChatMessage(data, 'chat-msg')
      } else {
        this.writeChatMessage(data, 'chat-msg-container')
      }
    })
    this.core.on('msgGlobal', (data) => {
      data.global = true
      this.writeChatMessage(data, 'chat-msg-container')
      this.lastUser = ''
    })
    this.core.on('msgHistory', (data) => {
      data.history = true
      if (data.user.name === this.lastUser) {
        this.writeChatMessage(data, 'chat-msg')
      } else {
        this.writeChatMessage(data, 'chat-msg-container')
      }
    })
    this.core.on('msgLinks', this.attachLinks.bind(this))
    this.core.on('msgWhisper', (data) => {
      this.writeChatMessage(data, 'chat-whisper')
    })
    this.core.on('myWhisper', (data) => {
      this.writeChatMessage(data, 'chat-my-whisper')
      const input = this.el.inputForm.find('.chat-input')
      input.val(`/w ${data.user.name} `)
      input.focus()
    })
    for (let evt of ['statusMsg', 'statusError', 'statusSuccess', 'statusInfo']) {
      this.core.on(evt, (data) => { this.statusEvent(data, evt.substr(6).toLowerCase()) })
    }

    // other chat events
    this.core.on('join', (user) => {
      this.renderUserList()
      Object.assign(user, this.core.userModeFlags(user.mode))
      const ignore = ChatOptions.get('Ignore joins and quits')
      if (ignore === 'Nobody' || (ignore === 'Guests' && user.loggedIn)) {
        this.statusEvent(`${user.name} has joined`, 'join')
      }
    })
    this.core.on('quit', (user) => {
      this.renderUserList()
      Object.assign(user, this.core.userModeFlags(user.mode))
      const ignore = ChatOptions.get('Ignore joins and quits')
      if (ignore === 'Nobody' || (ignore === 'Guests' && user.loggedIn)) {
        this.statusEvent(`${user.name} has quit`, 'quit')
      }
    })
    this.core.on('users', this.renderUserList.bind(this))
    this.core.on('away', this.renderUserList.bind(this))
    this.core.on('color', (data) => {
      this.statusEvent(`Color set to <span style="color:${data.color}">${data.color}</span>`, 'success')
      this.renderUserList()
    })
    this.core.on('chanmode', (data) => {
      const chanmodes = {
        m: 'moderated',
        k: 'key lock',
        r: 'registered users only',
        t: 'topic lock',
      }
      // split str to array, map letters to terms, filter out nulls
      const active = data.split('').map(k => chanmodes[k]).filter(k => k)
      this.el.chanmode.html(data ? `(${data})` : '')
      this.el.chanmode.prop('title', active.join(', '))
      this.statusEvent(`Mode set to ${data || '(none)'}`, this.chanmode ? 'success' : null)
      this.chanmode = data
    })
    this.core.on('topic', (data) => {
      const d = moment(data.time).format('MMM DD YYYY, HH:mm:ss')
      this.el.topic.html(data.msg)
      this.el.topic.prop('title', `${data.msg} (Set by ${data.user} on ${d})`)
      data.msg = `Topic: ${data.msg}`
      this.statusEvent(data)
      this.statusEvent(`Topic set by ${data.user} on ${d}`)
    })

    // core events
    this.core.on('command', (core, cmd) => {
      ChatCommands.runCommand(core, cmd, (c) => {
        this.statusEvent(`Unknown command: ${c}`, 'error')
      })
    })
    this.core.on('loadStream', (service, channel) => {
      let strim = null
      switch (service.toLowerCase()) {
        case 'picarto': strim = new PicartoPlayer(channel, this.core); break
        default: return this.statusEvent('Unknown streaming service.', 'error')
      }
      strim.start().catch((err) => {
        console.error(err)
        this.statusEvent(err.msg || err.message, 'error')
      })
    })

    // window etc. events
    ChatWindow.registerWindowEvents()
    const notificationEvents = ['msgRoom', 'msgWhisper', 'msgGlobal', 'join']
    for (let evt of notificationEvents) {
      this.core.on(evt, ChatWindow.unreadCounterHandler.bind(this))
      this.core.on(evt, ChatWindow.desktopNotificationsHandler.bind(this))
      this.core.on(evt, ChatWindow.audioNotificationsHandler.bind(this))
    }
    window.onresize = this.doAutoScroll.bind(this)
    // possible performance problem, but seems to work
    this.el.scrollContent.on('scroll', () => this.updateAutoScroll.bind(this))
  }

  writeChatMessage (data, type) {
    this.writeMessage(data, type)
    this.lastUser = data.user.name
    this.lastTimestamp = ChatFront.getTimestamp(data.time)
    this.lastMessage = data
  }

  attachLinks (data) {
    if (ChatOptions.get('Inline attachments') && data.links) {
      const chatMsg = this.el.ul.find(`[data-id="${data.id}"]`)
      if (chatMsg) {
        const urls = chatMsg.find('.chat-link').map((i, e) => e.href).get()
        for (let [i, link] of data.links.entries()) {
          link.url = urls[i]
        }
        data.links = ChatFront.parseAttachments(data.links)
        dust.render('chat-msg-attachments', data, (err, out) => {
          if (err) return console.error(err)
          const scroll = () => {
            this.el.scrollable.TrackpadScrollEmulator('recalculate')
            this.doAutoScroll()
          }
          $(out).find('img').on('load', scroll)
          chatMsg.find('.chat-attachments').html(out)
        })
      }
    }
  }

  renderUserList () {
    const userlist = this.core.userlist
    const list = {
      users: userlist.filter(user => user.loggedIn),
      guests: userlist.filter(user => !user.loggedIn),
    }
    this.el.userCount.html(list.users.length)
    this.el.guestCount.html(list.guests.length)
    dust.render('chat-userlist', list, (err, out) => {
      if (err) return console.error(err)
      this.el.users.html(out)
      this.el.aside.TrackpadScrollEmulator('recalculate')
    })
  }

  static parseMsgData (data) {
    data.rawMsg = data.msg
    data.timestamp = ChatFront.getTimestamp(data.time)
    data.msg = ChatFront.parseTags(data.msg)
    data.msg = ChatFront.parseLinks(data.msg)
    if (ChatOptions.get('Emotes')) {
      data.msg = ChatFront.parseEmotes(data.msg)
      data.msg = ChatFront.parseCarvings(data.msg)
    }
    return data
  }
  static getTimestamp (time) {
    const format = ChatOptions.get('Time format') === '12-hour' ? 'h:mma' : 'HH:mm'
    return moment(time).format(format)
  }
  static parseTags (msg) {
    msg.replace(
      // [s] or [spoiler]
      /\[s(?:poiler)?\](.*?)(?:\[\/s(?:poiler)?\]|$)/gi,
      '<span class="chat-spoiler">$1</span>'
    )
    return msg
  }
  static parseLinks (msg) {
    return msg.replace(
      /(https?:\/\/[\w_-]+\.[\w_-]+.*?)(?=\s|$)/gi,
      '<a href="$1" class="chat-link" target="_blank" rel="noreferrer">$1</a>'
    )
  }
  static parseAttachments (links) {
    if (links) {
      for (let link of links) {
        if (link.mime.includes('image/')) {
          link.image = true
        } else if (link.mime.includes('audio/')) {
          link.audio = true
        }
      }
    }
    return links
  }
  static parseEmotes (msg) {
    for (let [i, emote] of ChatFront.emotes) {
      msg = msg.replace(new RegExp(i, 'g'),
        `<img class="chat-emote chat-inline-img" src="${emote}" alt="${i}" title="${i}">'`)
    }
    return msg
  }
  static parseCarvings (msg) {
    for (let [i, carving] of ChatFront.carvings) {
      msg = msg.replace(new RegExp(`:${i}:`, 'g'),
        `<img class="chat-carving chat-inline-img" src="/images/carving_${i}.png" alt=":${i}:" title="${carving}" onclick="document.getElementById("audio_${i}").play()">`)
    }
    return msg
  }
}

ChatFront.emotes = new Map()
ChatFront.carvings = new Map(Object.entries({
  "hello": `This head says \"Hello\". Have another look. Do you sense the amicability in its eyes?`,
  "helpme": `This head says \"Help me!\". Look again. Can you hear the desperate plea?`,
  "imsorry": `This head says \"I&#39;m Sorry\". Have another look. Isn&#39;t that an expression of atonement?`,
  "thankyou": `This head says \"Thank you\". Have another look. Is this not the face of gratitude?`,
  "verygood": `This head says \"Very Good!\". Have another look. Does it not appear quite jovial? `,
}))
