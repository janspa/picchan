export default class ChatCore extends EventEmitter {
  static get SOCKET_HOST () { return window.location.host }
  static get EVENTS () {
    return [
      // 'connect', 'connect_error', 'disconnect', 'reconnect', 'reconnecting', 'error',
      'msgRoom', 'msgGlobal', 'msgHistory', 'msgLinks', 'msgWhisper',
      'statusMsg', 'statusError', 'statusInfo', 'statusSuccess',
      'join', 'quit', 'users', 'chanmode', 'topic', 'away', 'color',
    ]
  }

  constructor (opts = {}) {
    super()
    this.roomId = opts.roomId || null
    this.roomName = opts.roomName || ''
    this.commandChar = opts.commandChar || '/'
    this.socket = null
    this.usermap = new Map()
  }

  getUser (user) {
    if (typeof user === 'string') {
      user = { name: user }
    } else if (!user.name) {
      console.error(user)
      throw new TypeError(`Incorrect argument format`)
    }
    return this.usermap.get(user.name)
  }
  addUser (user) {
    if (!this.usermap.has(user.name)) {
      Object.assign(user, this.userModeFlags(user.mode))
      this.usermap.set(user.name, user)
    }
  }
  removeUser (user) {
    this.usermap.delete(user.name)
  }
  get userlist () {
    return Array.from(this.usermap.values())
  }

  connect () {
    return new Promise((resolve, reject) => {
      window.jQuery.get(`/room/${this.roomName}/token`)
        .done((token) => {
          this.socket = window.io(ChatCore.SOCKET_HOST, {
            // reconnectionDelay: 5000,
            reconnectionDelayMax: 15000,
            // reconnectionAttempts: 3,
            // forceNew: true,
            query: `token=${token}`
          })
          this.makeHandlers()
          resolve()
        })
        .fail(reject)
    })
  }

  makeHandlers () {
    // the special cases
    this.socket.on('error', (data) => {
      console.error(data)
    })
    this.socket.on('users', (data) => {
      this.usermap.clear()
      for (let user of data) {
        Object.assign(user, this.userModeFlags(user.mode))
        this.usermap.set(user.name, user)
      }
    })
    this.socket.on('join', (data) => {
      this.addUser(data)
    })
    this.socket.on('quit', (data) => {
      this.removeUser(data)
    })
    this.socket.on('away', (data) => {
      this.getUser(data.user).away = data.away
    })
    this.socket.on('color', (data) => {
      this.getUser(data.user).color = data.color
    })

    // give events generic handlers that trigger them forward
    const getUserEvents = ['msgRoom', 'msgGlobal', 'msgHistory', 'msgWhisper', 'away', 'color']
    const otherEvents = ChatCore.EVENTS.filter(evt => !getUserEvents.includes(evt))
    for (let evt of getUserEvents) {
      this.socket.on(evt, (data) => {
        if (this.getUser(data.user)) {
          data.user = this.getUser(data.user)
        }
        this.trigger(evt, [data])
      })
    }
    for (let evt of otherEvents) {
      this.socket.on(evt, (data) => { this.trigger(evt, [data]) })
    }
  }

  get isConnected () {
    return this.socket && this.socket.connected
  }

  send (evt, data) {
    if (this.isConnected) {
      this.socket.emit(evt, data)
    }
  }

  sendMessage (msg) {
    msg = msg.trim()
    if (msg) {
      if (msg.charAt(0) === this.commandChar) {
        if (msg.charAt(1) !== this.commandChar) {
          return this.trigger('command', [this, msg])
        } else {
          this.send('msgRoom', msg.substring(1))
        }
      } else {
        this.send('msgRoom', msg)
      }
      this.trigger('msgMy', [msg])
    }
  }

  sendWhisper (to, msg) {
    msg = msg.trim()
    if (msg) {
      this.send('msgWhisper', { to, msg })
      this.trigger('myWhisper', [{ user: this.getUser(to), msg }])
    }
  }

  statusMsg (msg) {
    this.trigger('statusMsg', [{msg, time: Date.now()}])
  }
  statusError (msg) {
    this.trigger('statusError', [{msg, time: Date.now()}])
  }
  statusSuccess (msg) {
    this.trigger('statusSuccess', [{msg, time: Date.now()}])
  }
  statusInfo (msg) {
    this.trigger('statusInfo', [{msg, time: Date.now()}])
  }

  loadStream (service, channel) {
    this.trigger('loadStream', [service, channel])
  }

  userModeFlags (mode) {
    return {
      loggedIn: (mode & 1) !== 0,
      mod:      (mode & 2) !== 0,
      owner:    (mode & 4) !== 0,
      admin:    (mode & 8) !== 0
    }
  }
}
