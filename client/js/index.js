import ChatFront from './chat-front'
import ChatOptions from './chat-options'

const $players = $('.players-container')
const $chat = $('.chat-container')
// const $chatMain = $('.chat-main')
// const $chatAside = $('.chat-aside')
// const $hideChat = $('#hide-chat')
const $hidePlayers = $('#hide-players')
const $hideUsers = $('#hide-users')
const $body = $('body')
const resizeMargin = 20

function setBorder (p) {
  $players.width(p)
  $players.css('right', p)
  $chat.css('left', p)
}

// initialize TSE
$('.tse-scrollable').TrackpadScrollEmulator({autoHide: false})
const instance = new ChatFront($chat)
instance.core.options = ChatOptions

ChatOptions.run()

$players.resizable({
  handles: 'e',
  minWidth: resizeMargin,
  maxWidth: $body.width() - resizeMargin,
}).on('resize', (evt, ui) => {
  const p = ($players.width() / $body.width() * 100)
  setBorder(p + '%')
}).on('resizestart', (evt, ui) => {
  // $hidePlayers.removeClass('inactive')
  // $hideChat.removeClass('inactive')
}).on('resizestop', (evt, ui) => {
  $('.tse-scrollable').TrackpadScrollEmulator('recalculate')
})

$(window).on('resize', () => {
  $players.resizable('option', 'maxWidth', $body.width() - resizeMargin)
  $('.tse-scrollable').TrackpadScrollEmulator('recalculate')
})

$hidePlayers.click(() => {
  const val = !$body.hasClass('hide-players')
  ChatOptions.set('Hide players', val)
  $('#modal-chat-options input[name="Hide players"]').prop('checked', val)
})

$hideUsers.click(() => {
  const val = !$body.hasClass('hide-userlist')
  ChatOptions.set('Hide userlist', val)
  $('#modal-chat-options input[name="Hide users"]').prop('checked', val)
})

$('.profile-modal').on('show.bs.modal', function (e) {
  const $el = $(e.relatedTarget)
  const $modal = $(this)
  const user = instance.core.getUser($el.data('name'))
  dust.render('chat-user-modal', user, (err, out) => {
    if (err) return console.error(err)
    $modal.find('.modal-content').html(out)
    $modal.find('.whisper-button').click(() => {
      $modal.modal('hide')
    })
  })
})
