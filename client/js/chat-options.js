const $body = $('body')
const $chat = $('.chat-main')
const $hidePlayers = $('.action-hide-players')
const $hideUsers = $('.action-hide-users')

const OPTS = [
  {
    set: 'Chat style',
    options: [
      {
        label: "Time format",
        description: "Hour time format for the message timestamps.",
        type: "radio",
        values: ["12-hour", "24-hour"],
        default: "24-hour",
      }, {
        label: "Emotes",
        description: "Show emotes in the chat.",
        type: "checkbox",
        default: true,
      }, {
        label: "Inline attachments",
        description: "Load links as attachments (currently just images) in the chat.",
        type: "checkbox",
        default: true,
      }, {
        label: "Avatar style",
        description: "The way the avatars are framed within the chat.",
        type: "radio",
        values: ["Square", "Rounded", "Circle"],
        default: "Rounded",
        fn: function (value) {
          $chat.toggleClass('rounded-avatars', value === "Rounded")
          $chat.toggleClass('circle-avatars', value === "Circle")
        },
      }, {
        label: "Reveal spoilers",
        description: "Automatically reveal text spoilers.",
        type: "checkbox",
        default: false,
        fn: function (value) {
          $body.toggleClass('reveal-spoilers', !!value)
        }
      },
    ]
  }, {
    set: 'Miscellaneous',
    options: [
      {
        label: "Hide players",
        description: "Hide the player panel.",
        type: "checkbox",
        default: false,
        fn: function (value, chat) {
          $body.toggleClass('hide-players', !!value)
          $hidePlayers.toggleClass('inactive', !!value)
          $('.tse-scrollable').TrackpadScrollEmulator('recalculate')
          if (chat) chat.doAutoScroll()
        }
      }, {
        label: "Hide userlist",
        description: "Hide the userlist.",
        type: "checkbox",
        default: false,
        fn: function (value, chat) {
          $body.toggleClass('hide-userlist', !!value)
          $hideUsers.toggleClass('inactive', !!value)
          $('.tse-scrollable').TrackpadScrollEmulator('recalculate')
          if (chat) chat.doAutoScroll()
        }
      }, {
        label: "Ignore joins and quits",
        description: "Stop the join and quit messages from appearing in the chat from certain users.",
        type: "radio",
        values: ["Nobody", "Guests", "Everybody"],
        default: "Guests",
      }, {
        label: "Sound notifications",
        description: "Play a sound on new messages when the chat window isn't active.",
        type: "checkbox",
        default: false,
        fn: function () { ChatOptions.instance.updateNotificationAudio() },
        options: [
          {
            label: "Audio file",
            description: "(Optional) The URL to the audio file you want to play.",
            type: "text",
            default: "",
            fn: function () { ChatOptions.instance.updateNotificationAudio() },
          }
        ]
      }, {
        label: "Desktop notifications",
        description: "Display a desktop notification on new messages when the chat window isn't active.",
        type: "checkbox",
        default: false,
        async: true,
        fn: function (request) {
          if (!request) {
            return Promise.resolve(false)
          } else if (typeof window.Notification === 'undefined') {
            window.alert("Your browser doesn't support desktop notifications.")
            return Promise.resolve(false)
          } else if (window.Notification.permission === 'granted') {
            return Promise.resolve(true)
          } else {
            return window.Notification.requestPermission().then((result) => {
              return (result === 'granted') ? Promise.resolve(true) : Promise.resolve(false)
            })
          }
        },
        options: [
          {
            label: "Notification timeout",
            description: "The time in seconds until the notification closes on its own.",
            type: "number",
            default: 4,
          }
        ]
      }, {
        label: "Ignored users",
        description: "A space-separated list of users whose messages you won't see.",
        type: "text",
        default: "",
        fn: function (value) {
          $('#modal-chat-options input[name="Ignored users"]').val(value.trim())
          ChatOptions.instance.parseIgnores()
        }
      }, {
        label: "Highlighted words",
        description: "A space-separated list of words that will cause a message to be highlighted in the chat.",
        type: "text",
        default: "",
        fn: function (value) {
          $('#modal-chat-options input[name="Highlighted words"]').val(value.trim())
          ChatOptions.instance.parseHilights()
        }
      }, {
        label: "Player tech",
        description: "The preference for the streaming tech used by the players. NOTE: no effect on already open players.",
        type: "radio",
        values: ["html5", "flash"],
        default: "html5",
        fn: function (value) {
          if (window.videojs) {
            window.videojs.options.techOrder.sort((a, b) => (a !== value))
          }
        }
      },
    ]
  },
  // {
  //   set: 'Local',
  //   options: []
  // }
]

let ignoredUsers = []
let hilightsRegex = []
let notificationAudio = null

class ChatOptions {
  constructor () {
    if (!ChatOptions.instance) {
      ChatOptions.instance = this
      this.options = {}
      this.handlers = {}
    }
    return ChatOptions.instance
  }

  get (key) {
    return this.options[key]
  }
  set (key, val) {
    this.options[key] = val
    return this.runHandler(key, val)
      .then((newVal) => {
        this.options[key] = (newVal != null) ? newVal : val
        this.save()
      })
  }
  softSet (key, val) {
    this.options[key] = val
  }
  save () {
    window.localStorage.setItem('options', JSON.stringify(this.options))
  }
  load () {
    const setDefaults = function (list) {
      for (let item of list) {
        if (item.label) {
          this.options[item.label] = item.default
          if (item.fn) {
            this.handlers[item.label] = { fn: item.fn, async: !!item.async }
          }
        }
        if (item.options) {
          setDefaults.call(this, item.options)
        }
      }
    }
    setDefaults.call(this, OPTS)
    if (window.localStorage.getItem('options')) {
      const storage = JSON.parse(window.localStorage.getItem('options'))
      for (let i in storage) {
        this.options[i] = storage[i]
      }
    } else {
      this.save()
    }
  }

  runHandler (key, ...values) {
    const handler = this.handlers[key]
    if (!handler) {
      return Promise.resolve()
    } else if (handler.async) {
      return handler.fn(...values)
    } else {
      try { return Promise.resolve(handler.fn(...values))
      } catch (e) { return Promise.reject(e) }
    }
  }

  isIgnored (name) {
    if (!name) return false
    return ignoredUsers.includes(name.toLowerCase())
  }
  isHilighted (msg) {
    if (!msg) return false
    return hilightsRegex.some((hilight) => hilight.test(msg))
  }
  hasIgnores () {
    return ignoredUsers.length > 0
  }
  hasHilights () {
    return hilightsRegex.length > 0
  }
  parseIgnores () {
    const str = this.get('Ignored users').trim().toLowerCase()
    ignoredUsers = (!str) ? [] : str.split(' ')
  }
  parseHilights () {
    const str = this.get('Highlighted words').trim()
    hilightsRegex = (!str) ? []
      : str.split(' ').map(hl => {
        hl = hl.replace(/\?/g, '\\w?')
        hl = hl.replace(/\*/g, '\\w*?')
        return new RegExp(`\\b${hl}\\b`, 'gi')
      })
  }
  updateNotificationAudio () {
    if (this.get('Sound notifications')) {
      const file = this.get('Audio file')
      const html = (!file)
        ? `<audio>
            <source src="/media/notify.ogg" type="audio/ogg">
            <source src="/media/notify.mp3" type="audio/mpeg">
            <source src="/media/notify.wav" type="audio/wav">
          </audio>`
        : `<audio><source src="${file}"</audio>`
      notificationAudio = $(html)[0]
    } else {
      notificationAudio = null
    }
  }
  playNotificationAudio () {
    if (notificationAudio != null) {
      notificationAudio.play()
    }
  }

  inputVal (container, name, val) {
    const input = container.find('input')
    switch (input.prop('type')) {
      case 'number':
      case 'text':
        if (val == null) return input.val()
        return input.val(val)
      case 'checkbox':
        if (val == null) return input.prop('checked')
        return input.prop('checked', val)
      case 'radio':
        if (val == null) return container.find(`input[name="${name}"]:checked`).val()
        return container.find(`input[value="${val}"]`).prop('checked', 'checked')
    }
  }

  run () {
    dust.render('options-container', OPTS, (err, out) => {
      if (err) {
        return console.error(err)
      }
      const $modal = $('#modal-chat-options .modal-body')
      $modal.html(out)
      $modal.find('[data-name]')
        .change((e, thing) => {
          e.preventDefault()
          e.stopPropagation()
          const $target = $(e.currentTarget)
          const name = $target.data('name')
          const val = this.inputVal($target, name)
          this.set(name, val)
            .then(() => { $target.attr('data-value', this.get(name)) })
        })
        .each((index, el) => {
          const $target = $(el)
          const name = $target.data('name')
          const val = this.get(name)
          this.inputVal($target, name, val)
          $target.attr('data-value', val)
          this.runHandler(name, val)
        })
    })
  }
}

const instance = new ChatOptions()
Object.freeze(instance)
instance.load()

export default instance
