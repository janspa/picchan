import ChatOptions from './chat-options'

// Registers events that keep track of the number of messages sent while the tab is not in focus
let originalTitle = document.title
let windowIsActive = true
let unreadMsgs = 0
let unreadHilight = false
let lastDesktopNotification = null
let notificationTimeout = null

function htmlDecode (text) {
  const d = document.createElement('div')
  d.innerHTML = text
  return (d.innerText || d.text || d.textContent)
}

export default class ChatWindow  {
  static registerWindowEvents () {
    originalTitle = document.title

    window.onblur = function () {
      windowIsActive = false
    }
    window.onfocus = function () {
      windowIsActive = true
      unreadMsgs = 0
      unreadHilight = false
      document.title = originalTitle
    }
  }

  static unreadCounterHandler (data) {
    if (!windowIsActive) {
      unreadMsgs++
      let title = `(${unreadMsgs}) ${originalTitle}`
      if (data.hilight || unreadHilight) {
        unreadHilight = true
        title = `! ${title}`
      }
      document.title = title
    }
  }

  static audioNotificationsHandler (data) {
    const user = data.user || data
    if (ChatOptions.isIgnored(user.name)) {
      return
    }
    if (!windowIsActive && ChatOptions.get('Sound notifications')) {
      ChatOptions.playNotificationAudio()
    }
  }

  static desktopNotificationsHandler (data) {
    const user = data.user || data
    if (ChatOptions.isIgnored(user.name)) {
      return
    }
    if (!windowIsActive &&
        ChatOptions.get("Desktop notifications") &&
        typeof window.Notification !== 'undefined' &&
        window.Notification.permission === 'granted') {
      if (notificationTimeout) {
        clearTimeout(notificationTimeout)
      }
      if (lastDesktopNotification != null) {
        lastDesktopNotification.close()
      }
      const opts = {
        body: user.name,
        icon: user.avatar,
      }
      if (data.rawMsg) {
        opts.body += `: ${htmlDecode(data.rawMsg)}`
      } else {
        // TODO: fix this
        // for now, assume no msg means this is a join
        opts.body += ' has joined'
      }
      let n = new window.Notification(originalTitle, opts)
      notificationTimeout = setTimeout(() => {
        n.close()
        n = null
      }, ChatOptions.get("Notification timeout") * 1000)
      lastDesktopNotification = n
    }
  }
}
