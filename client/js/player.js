if (window.videojs) {
  window.videojs.options.flash.swf = "/video-js.swf"
  window.videojs.options.techOrder = ['html5', 'flash']
}

const $players = $('.players')
const $playersContainer = $('.players-container')

class Player  {
  constructor(service, channel, chat) {
    this.service = service
    this.channel = channel
    this.wrapper = null
    this.chat = chat
    this.live = false
    this.autoClose = false
  }

  start () {
    return new Promise((resolve, reject) => {
      if ($(`#player_${this.service}_${this.channel}`).length > 0) {
        return reject(new Error(`A player for ${this.service} / ${this.channel} already exists`))
      }
      dust.render('player', this, (err, out) => {
        if (err) {
          return reject(err)
        }
        this.wrapper = $(out)
        this.wrapper.find('.player-menu-close').click(() => { this.close() })
        this.wrapper.find('.player-menu-expand').click(() => { this.wrapper.addClass('player-large') })
        this.wrapper.find('.player-menu-compress').click(() => { this.wrapper.removeClass('player-large') })
        this.wrapper[0].addEventListener('dragstart', Drag.handleDragStart, false)
        this.wrapper[0].addEventListener('dragenter', Drag.handleDragEnter, false)
        this.wrapper[0].addEventListener('dragleave', Drag.handleDragLeave, false)
        this.wrapper[0].addEventListener('dragover', Drag.handleDragOver, false)
        this.wrapper[0].addEventListener('dragend', Drag.handleDragEnd, false)
        this.wrapper[0].addEventListener('drop', Drag.handleDrop, false)

        $players.append(this.wrapper)
        return this.createPlayer()
          .then(() => {
            $playersContainer.TrackpadScrollEmulator('recalculate')
          })
      })
    })
  }

  createPlayer () {
    return new Promise((resolve, reject) => {
      if (!this.sources.html5 && !this.sources.flash) {
        return reject(new Error('No video source defined'))
      }
      dust.render('player-video', this, (err, out) => {
        if (err) {
          return reject(err)
        }
        this.wrapper.find('.video-wrapper').html(out)
        this.vjs = videojs(this.wrapper.find('video')[0], {
          controls: true,
          autoplay: true,
          poster: '/images/loading.jpg',
          bigPlayButton: false,
          textTrackDisplay: false,
          controlBar: {
            progressControl: false,
            remainingTimeDisplay: false,
          },
          notSupportedMessage: 'RIP strim',
        })
        $(this.vjs.posterImage.el_).addClass('d-block')
        this.vjs.on('ended', Player.onEnded.bind(this))
        this.vjs.on('loadedmetadata', Player.onStarted.bind(this))
        this.vjs.on('pause', Player.onPause.bind(this))
        this.vjs.on('error', Player.onError.bind(this))
        this.vjs.muted(true)
        // this.vjs.bigPlayButton.hide()
        this.vjs.fluid(true)
        return resolve()
      })
    })
  }

  close () {
    // this.vjs.pause()
    // setTimeout(() => {
      this.vjs.dispose()
      this.wrapper.remove()
      $playersContainer.TrackpadScrollEmulator('recalculate')
    // }, 100)
  }

  static onPause (e) {
    this.vjs.on('play', () => {
      this.vjs.off('play')
      this.vjs.load()
    })
  }
  static onStarted (e) {
    this.live = true
    if (this.chat) {
      this.chat.statusMsg(`Player ${this.service} / ${this.channel} has started.`)
    }
    $(this.vjs.posterImage.el_).removeClass('d-block')
  }
  static onEnded (e) {
    this.live = false
    if (this.chat) {
      this.chat.statusMsg(`${this.service} / ${this.channel} went offline.`)
    }
    if (this.autoClose) {
      this.close()
    }
  }
  static onError (e) {
    const err = this.vjs.error()
    console.error(err)
    if (err.code === 0) {
      err.message = "Something went wrong, the stream may or may not be offline. Hang on..."
      this.vjs.errorDisplay.update()
    } else if (err.code === 2) {
      if (this.chat) {
        this.chat.statusError(`A network error occured for ${this.service} / ${this.channel}`, 'error')
      }
      if (this.autoClose) {
        this.close()
      }
    } else if (err.code === 4) {
      if (this.live) {
        Player.onEnded.call(this)
      } else {
        if (this.chat) {
          this.chat.statusError(`No stream was found at ${this.service} / ${this.channel}`, 'error')
        }
        if (this.autoClose) {
          this.close()
        }
      }
    }
  }
}

class PicartoPlayer extends Player {
  constructor(channel, chat) {
    super('picarto', channel, chat)
  }
  start () {
    return new Promise((resolve, reject) => {
      $.get('/stream/picarto/balancinginfo', { channel: this.channel })
        .done((data) => {
          if (data === 'failedGetIP' || data === 'FULL') {
            return reject(new Error('Unable to get the stream.'))
          } else {
            this.sources = {
              html5: `https://${data}/hls/${this.channel}/index.m3u8`,
              flash: `https://${data}/hls/${this.channel}/index.m3u8`,
            }
            return super.start()
          }
        })
        .fail((data) => {
          return reject(new Error(data.responseText))
        })
    })
  }
}

class Drag {
  static handleDragStart (e) {
    Drag.srcEl = this
    e.dataTransfer.effectAllowed = 'move'
    e.dataTransfer.dropEffect = 'move'
    e.dataTransfer.setData('text', 'ayy')
    $(this).addClass('dragging')
  }
  static handleDragEnd (e) {
    $(this).removeClass('dragging')
    // it'd be nice to generalize this but oh well
    $('.player').removeClass('dragover')
  }
  static handleDragEnter (e) {
    $(this).addClass('dragover')
  }
  static handleDragLeave (e) {
    $(this).removeClass('dragover')
  }
  static handleDragOver (e) {
    e.preventDefault()
    return false
  }
  static handleDrop (e) {
    e.stopPropagation()
    e.preventDefault()
    if (Drag.srcEl !== this) {
      const temp = this.style.order
      this.style.order = Drag.srcEl.style.order
      Drag.srcEl.style.order = temp
    }
    return false
  }
}
Drag.srcEl = null

export { Player, PicartoPlayer }
