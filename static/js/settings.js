(function($) {
  /* http://axonflux.com/handy-rgb-to-hsl-and-rgb-to-hsv-color-model-c */
  function hslToRgb(h, s, l){
    var r, g, b;
    if (s == 0) {
      r = g = b = l;
    } else {
      var hue2rgb = function(p, q, t) {
        if(t < 0) t += 1;
        if(t > 1) t -= 1;
        if(t < 1/6) return p + (q - p) * 6 * t;
        if(t < 1/2) return q;
        if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
        return p;
      }
      var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      var p = 2 * l - q;
      r = hue2rgb(p, q, h + 1/3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1/3);
    }
    return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
  }
  // http://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
  function rgbToHex(rgb) {
    var val = rgb[2] | (rgb[1] << 8) | (rgb[0] << 16);
    return '#' + (0x1000000 + val).toString(16).slice(1);
  }

  var colorElement = $('.chatcolor');
  var colorInput = $('#color');
  var selectedColor = colorInput.val();
  var hoveredColor = colorInput.val();
  $('.colorpicker')
    .mousemove(function (e) {
      var hue = e.offsetX/$(this).width();
      if (hue < 0) hue = 0;
      if (hue > 1) hue = 1;
      var rgb = hslToRgb(hue,0.75,0.5);
      hoveredColor = rgb;
      colorElement.css('background', 'rgb('+rgb[0]+','+rgb[1]+','+rgb[2]+')');
      // colorInput.val();
    })
    .click(function (e) {
      var hex = rgbToHex(hoveredColor);
      selectedColor = hex;
      colorInput.val(hex);
    })
    .mouseout(function (e) {
      colorElement.css('background', selectedColor);
    });
  colorInput
    .change(function (e) {
      selectedColor = $(this).val();
      colorElement.css('background', selectedColor);
    });

})(jQuery);