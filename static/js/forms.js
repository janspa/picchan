function addAlert(msg, success) {
  msg = msg.split('\n').join('<br>');
  var ac = $('.alert-container').last();
  var html = '<div class="alert '+(success?'alert-success':'alert-danger')+' alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+msg+'</div>';
  ac.append(html);
}

function formSubmitter(opts) {
  opts = opts || {};
  return function(e) {
    e.preventDefault();
    $('.alert').removeClass('fade').alert('close');
    var btn = $(this).find('[type=submit]').prop('disabled', true);
    $.post(this.action, $(this).serialize(), null, 'json')
      .done(function (data) {
        if (data && data.message) {
          addAlert(data.message, true);
        }
        if (opts.done) {
          opts.done(data);
        }
        if (data.redirect) {
          window.location = data.redirect;
        }
      })
      .fail(function (data) {
        console.error(data);
        if (data.responseJSON) {
          addAlert(data.responseJSON.message, false);
        } else if (data.responseText) {
          addAlert(data.responseText, false);
        }
        if (opts.fail) {
          opts.fail(data);
        }
      })
      .always(function () {
        btn.prop('disabled', false);
        if (opts.always) {
          opts.always();
        }
    });
  };
}