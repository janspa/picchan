'use strict';
var chai = require('chai');
chai.use(require('chai-as-promised'));
chai.use(require('chai-things'));
chai.should();
var expect = chai.expect;
var db = require('../tasks/database-setup')('testing');
var Sequelize = require('sequelize');


describe('Database', function () {
  before('clear database', function (done) {
    db.clear().then(function () {
      done();
    });
  });

  var User = db.database.models.User;
  var data = [{
      name: 'mario',
      password: 'aaaaa1',
      password_confirm: 'aaaaa1',
  },{
    // empty object
  },{
      name: 'luigi',
      password: 'aaaaa1',
      password: 'bbbbb2',
  },{
      name: 'MARIO',
      password: 'aaaaa1',
      password_confirm: 'aaaaa1',
  }];

  it('should create a user with correct information', function () {
    return User.create(data[0]);
  });

  it('should not create a user with incomplete information', function () {
    return User.create(data[1])
      .catch(function (err) {
        expect(err).to.have.property('errors');
        // name validation should fail
        err.errors.should.contain.a.thing.with.property('path','name');
        // password validation should fail
        err.errors.should.contain.a.thing.with.property('path','password');
      });
  });

  it('should not create a user with incorrect password confirmation', function () {
    return User.create(data[2])
      .catch(function (err) {
        expect(err).to.have.deep.property('errors[0].path', 'passwordConfirmation');
      });
  });

  it('should create only unique case-insensitive usernames', function () {
    return User.create(data[3]).should.be.rejectedWith(Sequelize.UniqueConstraintError);
  });
});