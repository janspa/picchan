module.exports = {
  server: {
    address: process.env.NODE_IP,
    port: process.env.NODE_PORT,
    siteUrl: process.env.SITE_URL,
    secret: process.env.SECRET_KEY,
  },
  dataDir: process.env.DATA_DIR,
  database: {
    // sequelize: { dialect: 'mysql' }
  }
}
