const join = require('path').join

let config = module.exports = {
  server: {
    address: process.env.NODE_IP || '0.0.0.0',
    port: process.env.NODE_PORT || 3000,
    siteUrl: process.env.SITE_URL || 'localhost',
    secret: process.env.SECRET_KEY || 'override this',
  },

  dataDir: process.env.DATA_DIR || process.cwd(),

  database: {
    sequelize: {
      dialect: 'sqlite',
      username: null,
      password: null,
      get storage () { return join(config.dataDir, config.database.sqliteFile) },
      logging: false,
      operatorsAliases: false,
    },
    sqliteFile: 'picchan.sqlite3',
    adminDefaults: {
      name: 'admin',
      password: 'admin1',
      password_confirm: 'admin1',
      admin: true,
      active: true,
      email: 'admin@example.com',
      color: '#f9a3bd'
    },
  },
}
