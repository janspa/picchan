require('dotenv').config()

function deepClone (o1, o2) {
  for (let k in o2) {
    if (typeof o2[k] === 'object' && typeof o1[k] === 'object')
      deepClone(o1[k], o2[k])
    else
      o1[k] = o2[k]
  }
  return o1
}

const config = require('./default')
try { deepClone(config, require(`./${process.env.NODE_ENV || 'development'}`))
} catch (e) {}
try { deepClone(config, require('./local'))
} catch (e) {}
module.exports = config
