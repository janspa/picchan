'use strict'
console.log('[System] Starting')

const Promise = require('bluebird')
const config = require('./config')
const container = require('./container')

// config
container.register('config', [], config)

// modules
container.register('database',
  ['config'],
  require('./modules/database'))
container.register('server',
  ['config'],
  require('./modules/server'))
container.register('socket',
  ['server'],
  require('./modules/socket'))
container.register('chat',
  ['socket', 'database', 'config'],
  require('./modules/chat'))
container.register('router',
  ['server', 'database', 'chat'],
  require('./modules/router'))

let shuttingDown = false
const shutdown = () => {
  if (shuttingDown) return
  shuttingDown = true
  console.log('[System] Shutting down...')
  container.stop('chat')
  Promise.all([
    container.stop('server'),
    container.stop('database'),
  ])
    .then(() => {
      console.log('[System] Bye')
      process.exit()
    })
    .catch((err) => {
      console.error(err)
    })
}

// start
container.get('router')
container.startModule('database', { async: true })
  .then(() => {
    return container.startModule('server', { async: true })
  })
  .then(() => {
    return container.startModule('chat')
  })
  .then(() => {
    process.once('SIGUSR2', shutdown)
    process.once('SIGINT', shutdown)
    console.log('[System] Ready')
  })
  .catch({name:'SequelizeConnectionError'}, (err) => {
    console.error(`[Database] Error: ${err.message}`)
    if (err.original.code === 'SQLITE_CANTOPEN') {
      console.error("SQLite3 database hasn't been set up. Run `npm run init`.")
    }
  })
  .catch((err) => {
    console.error(err)
  })
