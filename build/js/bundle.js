(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var commandMap = {};
var commands = [];

var ChatCommands = {
  setCommand: function setCommand(cmds, description, fn, hidden) {
    var opts = { cmds: cmds, description: description, fn: fn, hidden: !!hidden };
    if (typeof opts.cmds === 'string') {
      opts.cmds = [opts.cmds];
    }
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = opts.cmds[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var c = _step.value;

        commandMap[c] = opts;
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }

    commands.push(opts);
  },

  runCommand: function runCommand(chat, msg, otherwise) {
    var split = msg.split(' ');
    var cmd = split[0].substring(1);
    if (commandMap[cmd]) {
      var args = split.slice(1);
      if (commandMap[cmd].fn(chat, args) === false) {
        ChatCommands.runCommand(chat, '/help ' + cmd);
      }
    } else if (otherwise) {
      otherwise(cmd);
    }
  }
};

ChatCommands.setCommand('help', 'Lists the available commands.', function (chat, args) {
  if (args.length > 0) {
    var c = commandMap[args[0]];
    if (c) {
      chat.statusInfo('<b>' + c.cmds.join(', ') + '</b> ' + c.description);
    }
  } else {
    var str = commands.filter(function (c) {
      return !c.hidden;
    }).map(function (c) {
      return '<b>' + c.cmds.join(', ') + '</b> ' + c.description;
    }).join('<br>');
    chat.statusInfo(str);
  }
});
ChatCommands.setCommand(['watch', 'picarto'], '[streamer ...] Embeds Picarto streamers to the player panel.', function (chat, args) {
  if (args.length > 0) {
    args.forEach(function (channel) {
      chat.loadStream('picarto', channel);
    });
  } else {
    return false;
  }
});
ChatCommands.setCommand(['whisper', 'w'], '[recipient] [message] Whispers a private message.', function (chat, args) {
  if (args.length > 1) {
    var to = args.shift();
    var msg = args.join(' ');
    chat.sendWhisper(to, msg);
  } else {
    return false;
  }
});
ChatCommands.setCommand('mode', '[+|-flags] Sets room modes. See <a href="/help#modes">help</a> for possible flags.', function (chat, args) {
  if (args.length > 0) {
    chat.socket.emit('chanmode', args[0]);
  } else {
    return false;
  }
});
ChatCommands.setCommand(['favorite', 'fav'], 'Adds the room to your favorites.', function (chat, args) {
  chat.socket.emit('favorite', true);
});
ChatCommands.setCommand(['unfavorite', 'unfav'], 'Removes the room from your favorites.', function (chat, args) {
  chat.socket.emit('favorite', false);
});
ChatCommands.setCommand('away', 'Sets or unsets your away status.', function (chat, args) {
  chat.socket.emit('away');
});
ChatCommands.setCommand('color', '[#xxxxxx] Changes the username color. Valid 6-digit hex color code (including #) required.', function (chat, args) {
  if (args.length > 0) {
    chat.socket.emit('color', args[0]);
  } else {
    return false;
  }
});
ChatCommands.setCommand(['hilight', 'highlight'], '[word ...] Adds words that will be highlighted in the chat. Available wildcards: ? for single characters (e.g. "ok?" matches "ok" and "oka" but not "okay") and * for globs (e.g. "*ok*" matches "bazooka").', function (chat, args) {
  if (args.length > 0) {
    var _iteratorNormalCompletion2 = true;
    var _didIteratorError2 = false;
    var _iteratorError2 = undefined;

    try {
      for (var _iterator2 = args[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
        var hilight = _step2.value;

        var oldHilights = chat.options.get('Highlighted words');
        chat.options.set('Highlighted words', (oldHilights + ' ' + hilight).trim());
        chat.statusSuccess('Added highlight: ' + hilight);
      }
    } catch (err) {
      _didIteratorError2 = true;
      _iteratorError2 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion2 && _iterator2.return) {
          _iterator2.return();
        }
      } finally {
        if (_didIteratorError2) {
          throw _iteratorError2;
        }
      }
    }
  } else {
    if (chat.options.hasHilights()) {
      chat.statusInfo('Hilighted words: ' + chat.options.get('Highlighted words').split(' ').join(', '));
    } else {
      chat.statusInfo('No highlighted words.');
    }
  }
});
ChatCommands.setCommand(['unhilight', 'unhighlight'], '[word ...] Removes words from the highlight list.', function (chat, args) {
  if (args.length > 0) {
    var _iteratorNormalCompletion3 = true;
    var _didIteratorError3 = false;
    var _iteratorError3 = undefined;

    try {
      for (var _iterator3 = args[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
        var hilight = _step3.value;

        var oldHilights = chat.options.get('Highlighted words');
        var index = oldHilights.toLowerCase().indexOf(hilight.toLowerCase());
        if (index >= 0) {
          var cutHilights = oldHilights.substr(0, index) + oldHilights.substr(index + hilight.length);
          chat.options.set('Highlighted words', cutHilights.trim());
          chat.statusSuccess('Removed highlight: ' + hilight);
        } else {
          chat.statusError(hilight + ' not found in the highlight list');
        }
      }
    } catch (err) {
      _didIteratorError3 = true;
      _iteratorError3 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion3 && _iterator3.return) {
          _iterator3.return();
        }
      } finally {
        if (_didIteratorError3) {
          throw _iteratorError3;
        }
      }
    }
  } else {
    return false;
  }
});
ChatCommands.setCommand('topic', '[message] Sets the room topic. 128 characters max.', function (chat, args) {
  if (args.length > 0) {
    chat.socket.emit('topic', args.join(' '));
  } else {
    return false;
  }
});
ChatCommands.setCommand('register', 'Registers the room.', function (chat, args) {
  chat.socket.emit('register');
});
ChatCommands.setCommand('unregister', 'Unregisters the room.', function (chat, args) {
  chat.socket.emit('unregister');
});
ChatCommands.setCommand('global', '[message] (Admin) Sends a global message.', function (chat, args) {
  chat.socket.emit('msgGlobal', args.join(' '));
}, true);

exports.default = ChatCommands;

},{}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ChatCore = function (_EventEmitter) {
  _inherits(ChatCore, _EventEmitter);

  _createClass(ChatCore, null, [{
    key: 'SOCKET_HOST',
    get: function get() {
      return window.location.host;
    }
  }, {
    key: 'EVENTS',
    get: function get() {
      return ['msgRoom', 'msgGlobal', 'msgHistory', 'msgLinks', 'msgWhisper', 'statusMsg', 'statusError', 'statusInfo', 'statusSuccess', 'join', 'quit', 'users', 'chanmode', 'topic', 'away', 'color'];
    }
  }]);

  function ChatCore() {
    var opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, ChatCore);

    var _this = _possibleConstructorReturn(this, (ChatCore.__proto__ || Object.getPrototypeOf(ChatCore)).call(this));

    _this.roomId = opts.roomId || null;
    _this.roomName = opts.roomName || '';
    _this.commandChar = opts.commandChar || '/';
    _this.socket = null;
    _this.usermap = new Map();
    return _this;
  }

  _createClass(ChatCore, [{
    key: 'getUser',
    value: function getUser(user) {
      if (typeof user === 'string') {
        user = { name: user };
      } else if (!user.name) {
        console.error(user);
        throw new TypeError('Incorrect argument format');
      }
      return this.usermap.get(user.name);
    }
  }, {
    key: 'addUser',
    value: function addUser(user) {
      if (!this.usermap.has(user.name)) {
        Object.assign(user, this.userModeFlags(user.mode));
        this.usermap.set(user.name, user);
      }
    }
  }, {
    key: 'removeUser',
    value: function removeUser(user) {
      this.usermap.delete(user.name);
    }
  }, {
    key: 'connect',
    value: function connect() {
      var _this2 = this;

      return new Promise(function (resolve, reject) {
        window.jQuery.get('/room/' + _this2.roomName + '/token').done(function (token) {
          _this2.socket = window.io(ChatCore.SOCKET_HOST, {
            reconnectionDelayMax: 15000,

            query: 'token=' + token
          });
          _this2.makeHandlers();
          resolve();
        }).fail(reject);
      });
    }
  }, {
    key: 'makeHandlers',
    value: function makeHandlers() {
      var _this3 = this;

      this.socket.on('error', function (data) {
        console.error(data);
      });
      this.socket.on('users', function (data) {
        _this3.usermap.clear();
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = data[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var user = _step.value;

            Object.assign(user, _this3.userModeFlags(user.mode));
            _this3.usermap.set(user.name, user);
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator.return) {
              _iterator.return();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }
      });
      this.socket.on('join', function (data) {
        _this3.addUser(data);
      });
      this.socket.on('quit', function (data) {
        _this3.removeUser(data);
      });
      this.socket.on('away', function (data) {
        _this3.getUser(data.user).away = data.away;
      });
      this.socket.on('color', function (data) {
        _this3.getUser(data.user).color = data.color;
      });

      var getUserEvents = ['msgRoom', 'msgGlobal', 'msgHistory', 'msgWhisper', 'away', 'color'];
      var otherEvents = ChatCore.EVENTS.filter(function (evt) {
        return !getUserEvents.includes(evt);
      });

      var _loop = function _loop(evt) {
        _this3.socket.on(evt, function (data) {
          if (_this3.getUser(data.user)) {
            data.user = _this3.getUser(data.user);
          }
          _this3.trigger(evt, [data]);
        });
      };

      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = getUserEvents[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var evt = _step2.value;

          _loop(evt);
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      var _loop2 = function _loop2(evt) {
        _this3.socket.on(evt, function (data) {
          _this3.trigger(evt, [data]);
        });
      };

      var _iteratorNormalCompletion3 = true;
      var _didIteratorError3 = false;
      var _iteratorError3 = undefined;

      try {
        for (var _iterator3 = otherEvents[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
          var evt = _step3.value;

          _loop2(evt);
        }
      } catch (err) {
        _didIteratorError3 = true;
        _iteratorError3 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion3 && _iterator3.return) {
            _iterator3.return();
          }
        } finally {
          if (_didIteratorError3) {
            throw _iteratorError3;
          }
        }
      }
    }
  }, {
    key: 'send',
    value: function send(evt, data) {
      if (this.isConnected) {
        this.socket.emit(evt, data);
      }
    }
  }, {
    key: 'sendMessage',
    value: function sendMessage(msg) {
      msg = msg.trim();
      if (msg) {
        if (msg.charAt(0) === this.commandChar) {
          if (msg.charAt(1) !== this.commandChar) {
            return this.trigger('command', [this, msg]);
          } else {
            this.send('msgRoom', msg.substring(1));
          }
        } else {
          this.send('msgRoom', msg);
        }
        this.trigger('msgMy', [msg]);
      }
    }
  }, {
    key: 'sendWhisper',
    value: function sendWhisper(to, msg) {
      msg = msg.trim();
      if (msg) {
        this.send('msgWhisper', { to: to, msg: msg });
        this.trigger('myWhisper', [{ user: this.getUser(to), msg: msg }]);
      }
    }
  }, {
    key: 'statusMsg',
    value: function statusMsg(msg) {
      this.trigger('statusMsg', [{ msg: msg, time: Date.now() }]);
    }
  }, {
    key: 'statusError',
    value: function statusError(msg) {
      this.trigger('statusError', [{ msg: msg, time: Date.now() }]);
    }
  }, {
    key: 'statusSuccess',
    value: function statusSuccess(msg) {
      this.trigger('statusSuccess', [{ msg: msg, time: Date.now() }]);
    }
  }, {
    key: 'statusInfo',
    value: function statusInfo(msg) {
      this.trigger('statusInfo', [{ msg: msg, time: Date.now() }]);
    }
  }, {
    key: 'loadStream',
    value: function loadStream(service, channel) {
      this.trigger('loadStream', [service, channel]);
    }
  }, {
    key: 'userModeFlags',
    value: function userModeFlags(mode) {
      return {
        loggedIn: (mode & 1) !== 0,
        mod: (mode & 2) !== 0,
        owner: (mode & 4) !== 0,
        admin: (mode & 8) !== 0
      };
    }
  }, {
    key: 'userlist',
    get: function get() {
      return Array.from(this.usermap.values());
    }
  }, {
    key: 'isConnected',
    get: function get() {
      return this.socket && this.socket.connected;
    }
  }]);

  return ChatCore;
}(EventEmitter);

exports.default = ChatCore;

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _chatCore = require('./chat-core');

var _chatCore2 = _interopRequireDefault(_chatCore);

var _chatOptions = require('./chat-options');

var _chatOptions2 = _interopRequireDefault(_chatOptions);

var _chatCommands = require('./chat-commands');

var _chatCommands2 = _interopRequireDefault(_chatCommands);

var _chatWindow = require('./chat-window');

var _chatWindow2 = _interopRequireDefault(_chatWindow);

var _player = require('./player');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ChatFront = function () {
  function ChatFront(container) {
    var _this = this;

    _classCallCheck(this, ChatFront);

    this.core = new _chatCore2.default({
      roomName: container.data('roomName'),
      roomId: container.data('roomId')
    });

    this.el = {};
    this.el.container = container;
    this.el.ul = container.find('.chat-ul');
    this.el.main = container.find('.chat-main');
    this.el.aside = container.find('.chat-aside');
    this.el.users = container.find('.chat-aside-userlist');
    this.el.topic = container.find('.chat-topic');
    this.el.chanmode = container.find('.chat-chanmode');
    this.el.userCount = container.find('.chat-user-count');
    this.el.guestCount = container.find('.chat-guest-count');
    this.el.inputForm = container.find('.chat-input-form');
    this.el.scrollable = this.el.ul.closest('.tse-scrollable');
    this.el.scrollContent = this.el.ul.closest('.tse-scroll-content');
    if (this.el.scrollContent.length === 0) {
      throw new Error("Initialize TSE before Chat");
    }

    this.myUsername = '';
    this.lastUser = '';
    this.lastTimestamp = null;
    this.inputHistory = [];
    this.autoScroll = true;

    this.core.connect().catch(function (err) {
      _this.core.statusError(err);
    }).then(function () {
      document.title = _this.roomName + ' - ' + document.title;
      _this.makeHandlers();
      _this.makeListeners();
      for (var c in ChatFront.carvings) {
        container.append('<audio id="audio_' + c + '" src="/media/' + c + '.mp3">');
      }
      return new Promise(function (resolve, reject) {
        window.jQuery.getJSON('/emotes.json').done(function (data) {
          ChatFront.emotes = new Map(Object.entries(data));
          resolve();
        }).fail(reject);
      });
    }).catch(function (err) {
      _this.core.statusError('Failed to load emotes.');
    });
  }

  _createClass(ChatFront, [{
    key: 'scrollToBottom',
    value: function scrollToBottom() {
      var c = this.el.scrollContent;
      c.stop();
      c.animate({ scrollTop: c.prop('scrollHeight') }, 'fast');
    }
  }, {
    key: 'updateAutoScroll',
    value: function updateAutoScroll() {
      this.autoScroll = this.isScrolledToBottom;
    }
  }, {
    key: 'doAutoScroll',
    value: function doAutoScroll() {
      if (this.autoScroll) {
        this.scrollToBottom();
      }
    }
  }, {
    key: 'writeMessage',
    value: function writeMessage(data, template) {
      var _this2 = this;

      if (data.user) {
        data.ignored = _chatOptions2.default.isIgnored(data.user.name || data.user);
        data.hilight = _chatOptions2.default.isHilighted(data.msg);
      }
      ChatFront.parseMsgData(data);
      if (template === 'chat-msg' && this.lastTimestamp === data.timestamp) {
        data.timestamp = null;
      } else if (template === 'chat-msg-container') {
        data.container = true;
      }
      dust.render(template, data, function (err, out) {
        if (err) return console.error(err);
        _this2.updateAutoScroll();
        if (template !== 'chat-msg') {
          _this2.lastUser = '';
          $(out).appendTo(_this2.el.ul);
        } else {
          var last = _this2.el.ul.find('li:last-child').find('.chat-msg:last');
          $(out).insertAfter(last);
        }
        _this2.el.scrollable.TrackpadScrollEmulator('recalculate');
        _this2.doAutoScroll();
        _this2.attachLinks(data);
      });
    }
  }, {
    key: 'statusEvent',
    value: function statusEvent(data, status) {
      if (typeof data === 'string') data = { msg: data };
      if (status) data.status = status;
      if (!data.time) data.time = Date.now();
      this.writeMessage(data, 'chat-statusmsg');
    }
  }, {
    key: 'makeListeners',
    value: function makeListeners() {
      var _this3 = this;

      this.el.inputForm.submit(function (e) {
        e.preventDefault();
        var input = $(e.target.firstChild);
        var msg = input.val();
        input.val('');
        _this3.core.sendMessage(msg);
      });
    }
  }, {
    key: 'makeHandlers',
    value: function makeHandlers() {
      var _this4 = this;

      this.core.socket.on('connect', function () {
        _this4.statusEvent('Connected to ' + _this4.roomName, 'join');
      });
      this.core.socket.on('connect_error', function (error) {
        if (_this4.core.socket.io.backoff.attempts <= 1) {
          console.error(error);
          _this4.statusEvent('Connection error: ' + (error.description.message || error.message), 'error');
        }
      });
      this.core.socket.on('disconnect', function () {
        _this4.statusEvent('Disconnected from ' + _this4.roomName, 'error');
      });
      this.core.socket.on('reconnecting', function (num) {
        if (num === 1) {
          _this4.statusEvent('Attempting to reconnect...');
        } else if (num === 2) {
          _this4.statusEvent('Still attempting to reconnect...');
        }
      });
      this.core.socket.on('error', function (err) {
        _this4.statusEvent(err, 'error');
      });

      this.core.on('msgRoom', function (data) {
        if (data.user.name === _this4.lastUser) {
          _this4.writeChatMessage(data, 'chat-msg');
        } else {
          _this4.writeChatMessage(data, 'chat-msg-container');
        }
      });
      this.core.on('msgGlobal', function (data) {
        data.global = true;
        _this4.writeChatMessage(data, 'chat-msg-container');
        _this4.lastUser = '';
      });
      this.core.on('msgHistory', function (data) {
        data.history = true;
        if (data.user.name === _this4.lastUser) {
          _this4.writeChatMessage(data, 'chat-msg');
        } else {
          _this4.writeChatMessage(data, 'chat-msg-container');
        }
      });
      this.core.on('msgLinks', this.attachLinks.bind(this));
      this.core.on('msgWhisper', function (data) {
        _this4.writeChatMessage(data, 'chat-whisper');
      });
      this.core.on('myWhisper', function (data) {
        _this4.writeChatMessage(data, 'chat-my-whisper');
        var input = _this4.el.inputForm.find('.chat-input');
        input.val('/w ' + data.user.name + ' ');
        input.focus();
      });

      var _loop = function _loop(evt) {
        _this4.core.on(evt, function (data) {
          _this4.statusEvent(data, evt.substr(6).toLowerCase());
        });
      };

      var _arr = ['statusMsg', 'statusError', 'statusSuccess', 'statusInfo'];
      for (var _i = 0; _i < _arr.length; _i++) {
        var evt = _arr[_i];
        _loop(evt);
      }

      this.core.on('join', function (user) {
        _this4.renderUserList();
        Object.assign(user, _this4.core.userModeFlags(user.mode));
        var ignore = _chatOptions2.default.get('Ignore joins and quits');
        if (ignore === 'Nobody' || ignore === 'Guests' && user.loggedIn) {
          _this4.statusEvent(user.name + ' has joined', 'join');
        }
      });
      this.core.on('quit', function (user) {
        _this4.renderUserList();
        Object.assign(user, _this4.core.userModeFlags(user.mode));
        var ignore = _chatOptions2.default.get('Ignore joins and quits');
        if (ignore === 'Nobody' || ignore === 'Guests' && user.loggedIn) {
          _this4.statusEvent(user.name + ' has quit', 'quit');
        }
      });
      this.core.on('users', this.renderUserList.bind(this));
      this.core.on('away', this.renderUserList.bind(this));
      this.core.on('color', function (data) {
        _this4.statusEvent('Color set to <span style="color:' + data.color + '">' + data.color + '</span>', 'success');
        _this4.renderUserList();
      });
      this.core.on('chanmode', function (data) {
        var chanmodes = {
          m: 'moderated',
          k: 'key lock',
          r: 'registered users only',
          t: 'topic lock'
        };

        var active = data.split('').map(function (k) {
          return chanmodes[k];
        }).filter(function (k) {
          return k;
        });
        _this4.el.chanmode.html(data ? '(' + data + ')' : '');
        _this4.el.chanmode.prop('title', active.join(', '));
        _this4.statusEvent('Mode set to ' + (data || '(none)'), _this4.chanmode ? 'success' : null);
        _this4.chanmode = data;
      });
      this.core.on('topic', function (data) {
        var d = moment(data.time).format('MMM DD YYYY, HH:mm:ss');
        _this4.el.topic.html(data.msg);
        _this4.el.topic.prop('title', data.msg + ' (Set by ' + data.user + ' on ' + d + ')');
        data.msg = 'Topic: ' + data.msg;
        _this4.statusEvent(data);
        _this4.statusEvent('Topic set by ' + data.user + ' on ' + d);
      });

      this.core.on('command', function (core, cmd) {
        _chatCommands2.default.runCommand(core, cmd, function (c) {
          _this4.statusEvent('Unknown command: ' + c, 'error');
        });
      });
      this.core.on('loadStream', function (service, channel) {
        var strim = null;
        switch (service.toLowerCase()) {
          case 'picarto':
            strim = new _player.PicartoPlayer(channel, _this4.core);break;
          default:
            return _this4.statusEvent('Unknown streaming service.', 'error');
        }
        strim.start().catch(function (err) {
          console.error(err);
          _this4.statusEvent(err.msg || err.message, 'error');
        });
      });

      _chatWindow2.default.registerWindowEvents();
      var notificationEvents = ['msgRoom', 'msgWhisper', 'msgGlobal', 'join'];
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = notificationEvents[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var evt = _step.value;

          this.core.on(evt, _chatWindow2.default.unreadCounterHandler.bind(this));
          this.core.on(evt, _chatWindow2.default.desktopNotificationsHandler.bind(this));
          this.core.on(evt, _chatWindow2.default.audioNotificationsHandler.bind(this));
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      window.onresize = this.doAutoScroll.bind(this);

      this.el.scrollContent.on('scroll', function () {
        return _this4.updateAutoScroll.bind(_this4);
      });
    }
  }, {
    key: 'writeChatMessage',
    value: function writeChatMessage(data, type) {
      this.writeMessage(data, type);
      this.lastUser = data.user.name;
      this.lastTimestamp = ChatFront.getTimestamp(data.time);
      this.lastMessage = data;
    }
  }, {
    key: 'attachLinks',
    value: function attachLinks(data) {
      var _this5 = this;

      if (_chatOptions2.default.get('Inline attachments') && data.links) {
        var chatMsg = this.el.ul.find('[data-id="' + data.id + '"]');
        if (chatMsg) {
          var urls = chatMsg.find('.chat-link').map(function (i, e) {
            return e.href;
          }).get();
          var _iteratorNormalCompletion2 = true;
          var _didIteratorError2 = false;
          var _iteratorError2 = undefined;

          try {
            for (var _iterator2 = data.links.entries()[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              var _ref = _step2.value;

              var _ref2 = _slicedToArray(_ref, 2);

              var i = _ref2[0];
              var link = _ref2[1];

              link.url = urls[i];
            }
          } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion2 && _iterator2.return) {
                _iterator2.return();
              }
            } finally {
              if (_didIteratorError2) {
                throw _iteratorError2;
              }
            }
          }

          data.links = ChatFront.parseAttachments(data.links);
          dust.render('chat-msg-attachments', data, function (err, out) {
            if (err) return console.error(err);
            var scroll = function scroll() {
              _this5.el.scrollable.TrackpadScrollEmulator('recalculate');
              _this5.doAutoScroll();
            };
            $(out).find('img').on('load', scroll);
            chatMsg.find('.chat-attachments').html(out);
          });
        }
      }
    }
  }, {
    key: 'renderUserList',
    value: function renderUserList() {
      var _this6 = this;

      var userlist = this.core.userlist;
      var list = {
        users: userlist.filter(function (user) {
          return user.loggedIn;
        }),
        guests: userlist.filter(function (user) {
          return !user.loggedIn;
        })
      };
      this.el.userCount.html(list.users.length);
      this.el.guestCount.html(list.guests.length);
      dust.render('chat-userlist', list, function (err, out) {
        if (err) return console.error(err);
        _this6.el.users.html(out);
        _this6.el.aside.TrackpadScrollEmulator('recalculate');
      });
    }
  }, {
    key: 'roomName',
    get: function get() {
      return this.core.roomName;
    }
  }, {
    key: 'isScrolledToBottom',
    get: function get() {
      var c = this.el.scrollContent;
      var margin = 10;
      return c.scrollTop() + c.height() > c.prop('scrollHeight') - margin;
    }
  }], [{
    key: 'parseMsgData',
    value: function parseMsgData(data) {
      data.rawMsg = data.msg;
      data.timestamp = ChatFront.getTimestamp(data.time);
      data.msg = ChatFront.parseTags(data.msg);
      data.msg = ChatFront.parseLinks(data.msg);
      if (_chatOptions2.default.get('Emotes')) {
        data.msg = ChatFront.parseEmotes(data.msg);
        data.msg = ChatFront.parseCarvings(data.msg);
      }
      return data;
    }
  }, {
    key: 'getTimestamp',
    value: function getTimestamp(time) {
      var format = _chatOptions2.default.get('Time format') === '12-hour' ? 'h:mma' : 'HH:mm';
      return moment(time).format(format);
    }
  }, {
    key: 'parseTags',
    value: function parseTags(msg) {
      msg.replace(/\[s(?:poiler)?\](.*?)(?:\[\/s(?:poiler)?\]|$)/gi, '<span class="chat-spoiler">$1</span>');
      return msg;
    }
  }, {
    key: 'parseLinks',
    value: function parseLinks(msg) {
      return msg.replace(/(https?:\/\/[\w_-]+\.[\w_-]+.*?)(?=\s|$)/gi, '<a href="$1" class="chat-link" target="_blank" rel="noreferrer">$1</a>');
    }
  }, {
    key: 'parseAttachments',
    value: function parseAttachments(links) {
      if (links) {
        var _iteratorNormalCompletion3 = true;
        var _didIteratorError3 = false;
        var _iteratorError3 = undefined;

        try {
          for (var _iterator3 = links[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
            var link = _step3.value;

            if (link.mime.includes('image/')) {
              link.image = true;
            } else if (link.mime.includes('audio/')) {
              link.audio = true;
            }
          }
        } catch (err) {
          _didIteratorError3 = true;
          _iteratorError3 = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion3 && _iterator3.return) {
              _iterator3.return();
            }
          } finally {
            if (_didIteratorError3) {
              throw _iteratorError3;
            }
          }
        }
      }
      return links;
    }
  }, {
    key: 'parseEmotes',
    value: function parseEmotes(msg) {
      var _iteratorNormalCompletion4 = true;
      var _didIteratorError4 = false;
      var _iteratorError4 = undefined;

      try {
        for (var _iterator4 = ChatFront.emotes[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
          var _ref3 = _step4.value;

          var _ref4 = _slicedToArray(_ref3, 2);

          var i = _ref4[0];
          var emote = _ref4[1];

          msg = msg.replace(new RegExp(i, 'g'), '<img class="chat-emote chat-inline-img" src="' + emote + '" alt="' + i + '" title="' + i + '">\'');
        }
      } catch (err) {
        _didIteratorError4 = true;
        _iteratorError4 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion4 && _iterator4.return) {
            _iterator4.return();
          }
        } finally {
          if (_didIteratorError4) {
            throw _iteratorError4;
          }
        }
      }

      return msg;
    }
  }, {
    key: 'parseCarvings',
    value: function parseCarvings(msg) {
      var _iteratorNormalCompletion5 = true;
      var _didIteratorError5 = false;
      var _iteratorError5 = undefined;

      try {
        for (var _iterator5 = ChatFront.carvings[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
          var _ref5 = _step5.value;

          var _ref6 = _slicedToArray(_ref5, 2);

          var i = _ref6[0];
          var carving = _ref6[1];

          msg = msg.replace(new RegExp(':' + i + ':', 'g'), '<img class="chat-carving chat-inline-img" src="/images/carving_' + i + '.png" alt=":' + i + ':" title="' + carving + '" onclick="document.getElementById("audio_' + i + '").play()">');
        }
      } catch (err) {
        _didIteratorError5 = true;
        _iteratorError5 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion5 && _iterator5.return) {
            _iterator5.return();
          }
        } finally {
          if (_didIteratorError5) {
            throw _iteratorError5;
          }
        }
      }

      return msg;
    }
  }]);

  return ChatFront;
}();

exports.default = ChatFront;


ChatFront.emotes = new Map();
ChatFront.carvings = new Map(Object.entries({
  "hello": 'This head says "Hello". Have another look. Do you sense the amicability in its eyes?',
  "helpme": 'This head says "Help me!". Look again. Can you hear the desperate plea?',
  "imsorry": 'This head says "I&#39;m Sorry". Have another look. Isn&#39;t that an expression of atonement?',
  "thankyou": 'This head says "Thank you". Have another look. Is this not the face of gratitude?',
  "verygood": 'This head says "Very Good!". Have another look. Does it not appear quite jovial? '
}));

},{"./chat-commands":1,"./chat-core":2,"./chat-options":4,"./chat-window":5,"./player":6}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var $body = $('body');
var $chat = $('.chat-main');
var $hidePlayers = $('.action-hide-players');
var $hideUsers = $('.action-hide-users');

var OPTS = [{
  set: 'Chat style',
  options: [{
    label: "Time format",
    description: "Hour time format for the message timestamps.",
    type: "radio",
    values: ["12-hour", "24-hour"],
    default: "24-hour"
  }, {
    label: "Emotes",
    description: "Show emotes in the chat.",
    type: "checkbox",
    default: true
  }, {
    label: "Inline attachments",
    description: "Load links as attachments (currently just images) in the chat.",
    type: "checkbox",
    default: true
  }, {
    label: "Avatar style",
    description: "The way the avatars are framed within the chat.",
    type: "radio",
    values: ["Square", "Rounded", "Circle"],
    default: "Rounded",
    fn: function fn(value) {
      $chat.toggleClass('rounded-avatars', value === "Rounded");
      $chat.toggleClass('circle-avatars', value === "Circle");
    }
  }, {
    label: "Reveal spoilers",
    description: "Automatically reveal text spoilers.",
    type: "checkbox",
    default: false,
    fn: function fn(value) {
      $body.toggleClass('reveal-spoilers', !!value);
    }
  }]
}, {
  set: 'Miscellaneous',
  options: [{
    label: "Hide players",
    description: "Hide the player panel.",
    type: "checkbox",
    default: false,
    fn: function fn(value, chat) {
      $body.toggleClass('hide-players', !!value);
      $hidePlayers.toggleClass('inactive', !!value);
      $('.tse-scrollable').TrackpadScrollEmulator('recalculate');
      if (chat) chat.doAutoScroll();
    }
  }, {
    label: "Hide userlist",
    description: "Hide the userlist.",
    type: "checkbox",
    default: false,
    fn: function fn(value, chat) {
      $body.toggleClass('hide-userlist', !!value);
      $hideUsers.toggleClass('inactive', !!value);
      $('.tse-scrollable').TrackpadScrollEmulator('recalculate');
      if (chat) chat.doAutoScroll();
    }
  }, {
    label: "Ignore joins and quits",
    description: "Stop the join and quit messages from appearing in the chat from certain users.",
    type: "radio",
    values: ["Nobody", "Guests", "Everybody"],
    default: "Guests"
  }, {
    label: "Sound notifications",
    description: "Play a sound on new messages when the chat window isn't active.",
    type: "checkbox",
    default: false,
    fn: function fn() {
      ChatOptions.instance.updateNotificationAudio();
    },
    options: [{
      label: "Audio file",
      description: "(Optional) The URL to the audio file you want to play.",
      type: "text",
      default: "",
      fn: function fn() {
        ChatOptions.instance.updateNotificationAudio();
      }
    }]
  }, {
    label: "Desktop notifications",
    description: "Display a desktop notification on new messages when the chat window isn't active.",
    type: "checkbox",
    default: false,
    async: true,
    fn: function fn(request) {
      if (!request) {
        return Promise.resolve(false);
      } else if (typeof window.Notification === 'undefined') {
        window.alert("Your browser doesn't support desktop notifications.");
        return Promise.resolve(false);
      } else if (window.Notification.permission === 'granted') {
        return Promise.resolve(true);
      } else {
        return window.Notification.requestPermission().then(function (result) {
          return result === 'granted' ? Promise.resolve(true) : Promise.resolve(false);
        });
      }
    },
    options: [{
      label: "Notification timeout",
      description: "The time in seconds until the notification closes on its own.",
      type: "number",
      default: 4
    }]
  }, {
    label: "Ignored users",
    description: "A space-separated list of users whose messages you won't see.",
    type: "text",
    default: "",
    fn: function fn(value) {
      $('#modal-chat-options input[name="Ignored users"]').val(value.trim());
      ChatOptions.instance.parseIgnores();
    }
  }, {
    label: "Highlighted words",
    description: "A space-separated list of words that will cause a message to be highlighted in the chat.",
    type: "text",
    default: "",
    fn: function fn(value) {
      $('#modal-chat-options input[name="Highlighted words"]').val(value.trim());
      ChatOptions.instance.parseHilights();
    }
  }, {
    label: "Player tech",
    description: "The preference for the streaming tech used by the players. NOTE: no effect on already open players.",
    type: "radio",
    values: ["html5", "flash"],
    default: "html5",
    fn: function fn(value) {
      if (window.videojs) {
        window.videojs.options.techOrder.sort(function (a, b) {
          return a !== value;
        });
      }
    }
  }]
}];

var ignoredUsers = [];
var hilightsRegex = [];
var notificationAudio = null;

var ChatOptions = function () {
  function ChatOptions() {
    _classCallCheck(this, ChatOptions);

    if (!ChatOptions.instance) {
      ChatOptions.instance = this;
      this.options = {};
      this.handlers = {};
    }
    return ChatOptions.instance;
  }

  _createClass(ChatOptions, [{
    key: 'get',
    value: function get(key) {
      return this.options[key];
    }
  }, {
    key: 'set',
    value: function set(key, val) {
      var _this = this;

      this.options[key] = val;
      return this.runHandler(key, val).then(function (newVal) {
        _this.options[key] = newVal != null ? newVal : val;
        _this.save();
      });
    }
  }, {
    key: 'softSet',
    value: function softSet(key, val) {
      this.options[key] = val;
    }
  }, {
    key: 'save',
    value: function save() {
      window.localStorage.setItem('options', JSON.stringify(this.options));
    }
  }, {
    key: 'load',
    value: function load() {
      var setDefaults = function setDefaults(list) {
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = list[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var item = _step.value;

            if (item.label) {
              this.options[item.label] = item.default;
              if (item.fn) {
                this.handlers[item.label] = { fn: item.fn, async: !!item.async };
              }
            }
            if (item.options) {
              setDefaults.call(this, item.options);
            }
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator.return) {
              _iterator.return();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }
      };
      setDefaults.call(this, OPTS);
      if (window.localStorage.getItem('options')) {
        var storage = JSON.parse(window.localStorage.getItem('options'));
        for (var i in storage) {
          this.options[i] = storage[i];
        }
      } else {
        this.save();
      }
    }
  }, {
    key: 'runHandler',
    value: function runHandler(key) {
      var handler = this.handlers[key];

      for (var _len = arguments.length, values = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        values[_key - 1] = arguments[_key];
      }

      if (!handler) {
        return Promise.resolve();
      } else if (handler.async) {
        return handler.fn.apply(handler, values);
      } else {
        try {
          return Promise.resolve(handler.fn.apply(handler, values));
        } catch (e) {
          return Promise.reject(e);
        }
      }
    }
  }, {
    key: 'isIgnored',
    value: function isIgnored(name) {
      if (!name) return false;
      return ignoredUsers.includes(name.toLowerCase());
    }
  }, {
    key: 'isHilighted',
    value: function isHilighted(msg) {
      if (!msg) return false;
      return hilightsRegex.some(function (hilight) {
        return hilight.test(msg);
      });
    }
  }, {
    key: 'hasIgnores',
    value: function hasIgnores() {
      return ignoredUsers.length > 0;
    }
  }, {
    key: 'hasHilights',
    value: function hasHilights() {
      return hilightsRegex.length > 0;
    }
  }, {
    key: 'parseIgnores',
    value: function parseIgnores() {
      var str = this.get('Ignored users').trim().toLowerCase();
      ignoredUsers = !str ? [] : str.split(' ');
    }
  }, {
    key: 'parseHilights',
    value: function parseHilights() {
      var str = this.get('Highlighted words').trim();
      hilightsRegex = !str ? [] : str.split(' ').map(function (hl) {
        hl = hl.replace(/\?/g, '\\w?');
        hl = hl.replace(/\*/g, '\\w*?');
        return new RegExp('\\b' + hl + '\\b', 'gi');
      });
    }
  }, {
    key: 'updateNotificationAudio',
    value: function updateNotificationAudio() {
      if (this.get('Sound notifications')) {
        var file = this.get('Audio file');
        var html = !file ? '<audio>\n            <source src="/media/notify.ogg" type="audio/ogg">\n            <source src="/media/notify.mp3" type="audio/mpeg">\n            <source src="/media/notify.wav" type="audio/wav">\n          </audio>' : '<audio><source src="' + file + '"</audio>';
        notificationAudio = $(html)[0];
      } else {
        notificationAudio = null;
      }
    }
  }, {
    key: 'playNotificationAudio',
    value: function playNotificationAudio() {
      if (notificationAudio != null) {
        notificationAudio.play();
      }
    }
  }, {
    key: 'inputVal',
    value: function inputVal(container, name, val) {
      var input = container.find('input');
      switch (input.prop('type')) {
        case 'number':
        case 'text':
          if (val == null) return input.val();
          return input.val(val);
        case 'checkbox':
          if (val == null) return input.prop('checked');
          return input.prop('checked', val);
        case 'radio':
          if (val == null) return container.find('input[name="' + name + '"]:checked').val();
          return container.find('input[value="' + val + '"]').prop('checked', 'checked');
      }
    }
  }, {
    key: 'run',
    value: function run() {
      var _this2 = this;

      dust.render('options-container', OPTS, function (err, out) {
        if (err) {
          return console.error(err);
        }
        var $modal = $('#modal-chat-options .modal-body');
        $modal.html(out);
        $modal.find('[data-name]').change(function (e, thing) {
          e.preventDefault();
          e.stopPropagation();
          var $target = $(e.currentTarget);
          var name = $target.data('name');
          var val = _this2.inputVal($target, name);
          _this2.set(name, val).then(function () {
            $target.attr('data-value', _this2.get(name));
          });
        }).each(function (index, el) {
          var $target = $(el);
          var name = $target.data('name');
          var val = _this2.get(name);
          _this2.inputVal($target, name, val);
          $target.attr('data-value', val);
          _this2.runHandler(name, val);
        });
      });
    }
  }]);

  return ChatOptions;
}();

var instance = new ChatOptions();
Object.freeze(instance);
instance.load();

exports.default = instance;

},{}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _chatOptions = require('./chat-options');

var _chatOptions2 = _interopRequireDefault(_chatOptions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var originalTitle = document.title;
var windowIsActive = true;
var unreadMsgs = 0;
var unreadHilight = false;
var lastDesktopNotification = null;
var notificationTimeout = null;

function htmlDecode(text) {
  var d = document.createElement('div');
  d.innerHTML = text;
  return d.innerText || d.text || d.textContent;
}

var ChatWindow = function () {
  function ChatWindow() {
    _classCallCheck(this, ChatWindow);
  }

  _createClass(ChatWindow, null, [{
    key: 'registerWindowEvents',
    value: function registerWindowEvents() {
      originalTitle = document.title;

      window.onblur = function () {
        windowIsActive = false;
      };
      window.onfocus = function () {
        windowIsActive = true;
        unreadMsgs = 0;
        unreadHilight = false;
        document.title = originalTitle;
      };
    }
  }, {
    key: 'unreadCounterHandler',
    value: function unreadCounterHandler(data) {
      if (!windowIsActive) {
        unreadMsgs++;
        var title = '(' + unreadMsgs + ') ' + originalTitle;
        if (data.hilight || unreadHilight) {
          unreadHilight = true;
          title = '! ' + title;
        }
        document.title = title;
      }
    }
  }, {
    key: 'audioNotificationsHandler',
    value: function audioNotificationsHandler(data) {
      var user = data.user || data;
      if (_chatOptions2.default.isIgnored(user.name)) {
        return;
      }
      if (!windowIsActive && _chatOptions2.default.get('Sound notifications')) {
        _chatOptions2.default.playNotificationAudio();
      }
    }
  }, {
    key: 'desktopNotificationsHandler',
    value: function desktopNotificationsHandler(data) {
      var user = data.user || data;
      if (_chatOptions2.default.isIgnored(user.name)) {
        return;
      }
      if (!windowIsActive && _chatOptions2.default.get("Desktop notifications") && typeof window.Notification !== 'undefined' && window.Notification.permission === 'granted') {
        if (notificationTimeout) {
          clearTimeout(notificationTimeout);
        }
        if (lastDesktopNotification != null) {
          lastDesktopNotification.close();
        }
        var opts = {
          body: user.name,
          icon: user.avatar
        };
        if (data.rawMsg) {
          opts.body += ': ' + htmlDecode(data.rawMsg);
        } else {
          opts.body += ' has joined';
        }
        var n = new window.Notification(originalTitle, opts);
        notificationTimeout = setTimeout(function () {
          n.close();
          n = null;
        }, _chatOptions2.default.get("Notification timeout") * 1000);
        lastDesktopNotification = n;
      }
    }
  }]);

  return ChatWindow;
}();

exports.default = ChatWindow;

},{"./chat-options":4}],6:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

if (window.videojs) {
  window.videojs.options.flash.swf = "/video-js.swf";
  window.videojs.options.techOrder = ['html5', 'flash'];
}

var $players = $('.players');
var $playersContainer = $('.players-container');

var Player = function () {
  function Player(service, channel, chat) {
    _classCallCheck(this, Player);

    this.service = service;
    this.channel = channel;
    this.wrapper = null;
    this.chat = chat;
    this.live = false;
    this.autoClose = false;
  }

  _createClass(Player, [{
    key: 'start',
    value: function start() {
      var _this = this;

      return new Promise(function (resolve, reject) {
        if ($('#player_' + _this.service + '_' + _this.channel).length > 0) {
          return reject(new Error('A player for ' + _this.service + ' / ' + _this.channel + ' already exists'));
        }
        dust.render('player', _this, function (err, out) {
          if (err) {
            return reject(err);
          }
          _this.wrapper = $(out);
          _this.wrapper.find('.player-menu-close').click(function () {
            _this.close();
          });
          _this.wrapper.find('.player-menu-expand').click(function () {
            _this.wrapper.addClass('player-large');
          });
          _this.wrapper.find('.player-menu-compress').click(function () {
            _this.wrapper.removeClass('player-large');
          });
          _this.wrapper[0].addEventListener('dragstart', Drag.handleDragStart, false);
          _this.wrapper[0].addEventListener('dragenter', Drag.handleDragEnter, false);
          _this.wrapper[0].addEventListener('dragleave', Drag.handleDragLeave, false);
          _this.wrapper[0].addEventListener('dragover', Drag.handleDragOver, false);
          _this.wrapper[0].addEventListener('dragend', Drag.handleDragEnd, false);
          _this.wrapper[0].addEventListener('drop', Drag.handleDrop, false);

          $players.append(_this.wrapper);
          return _this.createPlayer().then(function () {
            $playersContainer.TrackpadScrollEmulator('recalculate');
          });
        });
      });
    }
  }, {
    key: 'createPlayer',
    value: function createPlayer() {
      var _this2 = this;

      return new Promise(function (resolve, reject) {
        if (!_this2.sources.html5 && !_this2.sources.flash) {
          return reject(new Error('No video source defined'));
        }
        dust.render('player-video', _this2, function (err, out) {
          if (err) {
            return reject(err);
          }
          _this2.wrapper.find('.video-wrapper').html(out);
          _this2.vjs = videojs(_this2.wrapper.find('video')[0], {
            controls: true,
            autoplay: true,
            poster: '/images/loading.jpg',
            bigPlayButton: false,
            textTrackDisplay: false,
            controlBar: {
              progressControl: false,
              remainingTimeDisplay: false
            },
            notSupportedMessage: 'RIP strim'
          });
          $(_this2.vjs.posterImage.el_).addClass('d-block');
          _this2.vjs.on('ended', Player.onEnded.bind(_this2));
          _this2.vjs.on('loadedmetadata', Player.onStarted.bind(_this2));
          _this2.vjs.on('pause', Player.onPause.bind(_this2));
          _this2.vjs.on('error', Player.onError.bind(_this2));
          _this2.vjs.muted(true);

          _this2.vjs.fluid(true);
          return resolve();
        });
      });
    }
  }, {
    key: 'close',
    value: function close() {
      this.vjs.dispose();
      this.wrapper.remove();
      $playersContainer.TrackpadScrollEmulator('recalculate');
    }
  }], [{
    key: 'onPause',
    value: function onPause(e) {
      var _this3 = this;

      this.vjs.on('play', function () {
        _this3.vjs.off('play');
        _this3.vjs.load();
      });
    }
  }, {
    key: 'onStarted',
    value: function onStarted(e) {
      this.live = true;
      if (this.chat) {
        this.chat.statusMsg('Player ' + this.service + ' / ' + this.channel + ' has started.');
      }
      $(this.vjs.posterImage.el_).removeClass('d-block');
    }
  }, {
    key: 'onEnded',
    value: function onEnded(e) {
      this.live = false;
      if (this.chat) {
        this.chat.statusMsg(this.service + ' / ' + this.channel + ' went offline.');
      }
      if (this.autoClose) {
        this.close();
      }
    }
  }, {
    key: 'onError',
    value: function onError(e) {
      var err = this.vjs.error();
      console.error(err);
      if (err.code === 0) {
        err.message = "Something went wrong, the stream may or may not be offline. Hang on...";
        this.vjs.errorDisplay.update();
      } else if (err.code === 2) {
        if (this.chat) {
          this.chat.statusError('A network error occured for ' + this.service + ' / ' + this.channel, 'error');
        }
        if (this.autoClose) {
          this.close();
        }
      } else if (err.code === 4) {
        if (this.live) {
          Player.onEnded.call(this);
        } else {
          if (this.chat) {
            this.chat.statusError('No stream was found at ' + this.service + ' / ' + this.channel, 'error');
          }
          if (this.autoClose) {
            this.close();
          }
        }
      }
    }
  }]);

  return Player;
}();

var PicartoPlayer = function (_Player) {
  _inherits(PicartoPlayer, _Player);

  function PicartoPlayer(channel, chat) {
    _classCallCheck(this, PicartoPlayer);

    return _possibleConstructorReturn(this, (PicartoPlayer.__proto__ || Object.getPrototypeOf(PicartoPlayer)).call(this, 'picarto', channel, chat));
  }

  _createClass(PicartoPlayer, [{
    key: 'start',
    value: function start() {
      var _this5 = this;

      return new Promise(function (resolve, reject) {
        $.get('/stream/picarto/balancinginfo', { channel: _this5.channel }).done(function (data) {
          if (data === 'failedGetIP' || data === 'FULL') {
            return reject(new Error('Unable to get the stream.'));
          } else {
            _this5.sources = {
              html5: 'https://' + data + '/hls/' + _this5.channel + '/index.m3u8',
              flash: 'https://' + data + '/hls/' + _this5.channel + '/index.m3u8'
            };
            return _get(PicartoPlayer.prototype.__proto__ || Object.getPrototypeOf(PicartoPlayer.prototype), 'start', _this5).call(_this5);
          }
        }).fail(function (data) {
          return reject(new Error(data.responseText));
        });
      });
    }
  }]);

  return PicartoPlayer;
}(Player);

var Drag = function () {
  function Drag() {
    _classCallCheck(this, Drag);
  }

  _createClass(Drag, null, [{
    key: 'handleDragStart',
    value: function handleDragStart(e) {
      Drag.srcEl = this;
      e.dataTransfer.effectAllowed = 'move';
      e.dataTransfer.dropEffect = 'move';
      e.dataTransfer.setData('text', 'ayy');
      $(this).addClass('dragging');
    }
  }, {
    key: 'handleDragEnd',
    value: function handleDragEnd(e) {
      $(this).removeClass('dragging');

      $('.player').removeClass('dragover');
    }
  }, {
    key: 'handleDragEnter',
    value: function handleDragEnter(e) {
      $(this).addClass('dragover');
    }
  }, {
    key: 'handleDragLeave',
    value: function handleDragLeave(e) {
      $(this).removeClass('dragover');
    }
  }, {
    key: 'handleDragOver',
    value: function handleDragOver(e) {
      e.preventDefault();
      return false;
    }
  }, {
    key: 'handleDrop',
    value: function handleDrop(e) {
      e.stopPropagation();
      e.preventDefault();
      if (Drag.srcEl !== this) {
        var temp = this.style.order;
        this.style.order = Drag.srcEl.style.order;
        Drag.srcEl.style.order = temp;
      }
      return false;
    }
  }]);

  return Drag;
}();

Drag.srcEl = null;

exports.Player = Player;
exports.PicartoPlayer = PicartoPlayer;

},{}],7:[function(require,module,exports){
'use strict';

var _chatFront = require('./chat-front');

var _chatFront2 = _interopRequireDefault(_chatFront);

var _chatOptions = require('./chat-options');

var _chatOptions2 = _interopRequireDefault(_chatOptions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $players = $('.players-container');
var $chat = $('.chat-container');

var $hidePlayers = $('#hide-players');
var $hideUsers = $('#hide-users');
var $body = $('body');
var resizeMargin = 20;

function setBorder(p) {
  $players.width(p);
  $players.css('right', p);
  $chat.css('left', p);
}

$('.tse-scrollable').TrackpadScrollEmulator({ autoHide: false });
var instance = new _chatFront2.default($chat);
instance.core.options = _chatOptions2.default;

_chatOptions2.default.run();

$players.resizable({
  handles: 'e',
  minWidth: resizeMargin,
  maxWidth: $body.width() - resizeMargin
}).on('resize', function (evt, ui) {
  var p = $players.width() / $body.width() * 100;
  setBorder(p + '%');
}).on('resizestart', function (evt, ui) {}).on('resizestop', function (evt, ui) {
  $('.tse-scrollable').TrackpadScrollEmulator('recalculate');
});

$(window).on('resize', function () {
  $players.resizable('option', 'maxWidth', $body.width() - resizeMargin);
  $('.tse-scrollable').TrackpadScrollEmulator('recalculate');
});

$hidePlayers.click(function () {
  var val = !$body.hasClass('hide-players');
  _chatOptions2.default.set('Hide players', val);
  $('#modal-chat-options input[name="Hide players"]').prop('checked', val);
});

$hideUsers.click(function () {
  var val = !$body.hasClass('hide-userlist');
  _chatOptions2.default.set('Hide userlist', val);
  $('#modal-chat-options input[name="Hide users"]').prop('checked', val);
});

$('.profile-modal').on('show.bs.modal', function (e) {
  var $el = $(e.relatedTarget);
  var $modal = $(this);
  var user = instance.core.getUser($el.data('name'));
  dust.render('chat-user-modal', user, function (err, out) {
    if (err) return console.error(err);
    $modal.find('.modal-content').html(out);
    $modal.find('.whisper-button').click(function () {
      $modal.modal('hide');
    });
  });
});

},{"./chat-front":3,"./chat-options":4}]},{},[7]);
