'use strict';
var router = require('express').Router();

module.exports = function(app, db, chat) {

  // index
  router.get('/', function(req, res) {
    db.models.NewsArticle.findAll({
      order: 'createdAt DESC'
    })
    .then(function (news) {
      res.render('home', {
        articles: news
      });
    })
    .catch(function (err) {
      res.render('home', {});
    })
  });

  return router;
};