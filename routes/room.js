'use strict'
const router = require('express').Router()

module.exports = (app, db, chat) => {
  router.get('/:roomname', (req, res) => {
    db.models.Room.findOne({
      // where: db.whereNocase('name', req.params.roomname)
      where: { 'name': req.params.roomname.toLowerCase() }
    }).then((room) => {
      if (!room) {
        room = { name: req.params.roomname }
      }
      res.render('room', {
        room: room,
        bodyClasses: 'page-room',
        title: `${res.locals.title} - ${room.name}`,
      })
    })
  })

  router.get('/:roomname/token', (req, res) => {
    const username = res.locals.user ? res.locals.user.name : null
    res.send(chat.manager.createToken(req.params.roomname, username))
  })

  return router
}
