'use strict';
var request = require('request');
var router = require('express').Router();

module.exports = function (app, db, chat) {

  router.get('/picarto/balancinginfo', function(req, res) {
    if (!req.query.channel) {
      return res.status(400).send("Query parameter 'channel' is required.");
    }
    var url = 'https://picarto.tv/process/channel';
    var data = { loadbalancinginfo: req.query.channel };
    return request.post({url: url, form: data}, function (error, response, body) {
      return res.status(response.statusCode).send(body || error);
    });
  });

  return router;
};