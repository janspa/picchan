'use strict';
var httpError = require('http-errors');
var request = require('request');
var bodyParser = require('body-parser');
var router = require('express').Router();
var Op = require('sequelize').Op;

module.exports = function (app, db) {

  function getPicartoCode(user) {
    return '{'+(user.id+'').substr(0,8)+'}';
  }

  router.use(function (req, res, next) {
    if (!req.session.userId || !res.locals.user) {
      return next(new httpError.Unauthorized());
    }
    next();
  });
  router.use('/admin', function (req, res, next) {
    if (!res.locals.user.admin) {
      return next(new httpError.Unauthorized());
    }
    next();
  });

  router.get('/', function(req, res) {
    res.redirect(req.baseUrl+'/profile');
  });

  router.get('/profile', function(req, res) {
    res.render('settings-profile', {
    });
  });

  router.post('/user', bodyParser.urlencoded({extended:true}), function(req, res) {
    var user = res.locals.user;
    user.update(req.body, {
      fields: ['avatar','color']
    })
    .then(function () {
      return res.status(200).send({
        message: 'Settings saved.',
        user: user,
      });
    })
    .catch(function (err) {
      res.status(500).send({
        message: err.message,
        user: user.previous(),
      });
    });
  });
  router.post('/password', bodyParser.urlencoded({extended:true}), function(req, res) {
    var user = res.locals.user;
    user.verifyPassword(req.body.oldpassword, function (err, isMatch) {
      if (err) {
        return res.status(500).send({
          message: err,
        });
      }
      if (isMatch !== true) {
        return res.status(401).send({
          message: 'Incorrect password.',
        });
      }
      // console.log(req.body)
      user.update({
        password: req.body.newpassword,
        password_confirm: req.body.newpassword2
      })
      .then(function () {
        return res.status(200).send({
          message: 'Password updated.',
          user: user,
        });
      })
      .catch(function (err) {
        return res.status(500).send({
          message: err.message
        });
      });
    });
  });
  router.post('/email', bodyParser.urlencoded({extended:true}), function(req, res) {
    var user = res.locals.user;
    user.verifyPassword(req.body.password, function (err, isMatch) {
      if (err) {
        return res.status(500).send({
          message: err,
        });
      }
      if (isMatch !== true) {
        return res.status(401).send({
          message: 'Incorrect password.',
        });
      }
      user.update({
        email: req.body.newemail,
      })
      .then(function () {
        return res.status(200).send({
          message: 'Email updated.',
          user: user,
        });
      })
      .catch(function (err) {
        return res.status(500).send({
          message: err.message
        });
      });
    });
  });

  router.get('/account', function(req, res) {
    res.render('settings-account', {
    });
  });

  router.get('/favorites', function(req, res) {
    res.locals.user.getFavorites()
    .then(function (favorites) {
      res.render('settings-favorites', {
        favorites: favorites
      });
      return null;
    })
    .catch(function (err) {
      return res.status(500).send({
        message: err.message,
      });
    });
  });

  router.get('/connections', function(req, res) {
    res.render('settings-connections', {
      picartoCode: getPicartoCode(res.locals.user),
    });
  });
  router.post('/connections', bodyParser.urlencoded({extended:true}), function(req, res) {
    // if connecting picarto to the account
    if (req.body.connect_picarto && req.body.connect_picarto_username) {
      // request the given user's picarto channel page
      return request('http://picarto.tv/'+req.body.connect_picarto_username, function (error, response, body) {
        if (!error && response.statusCode === 200) {
          // if the code is there
          if (body.indexOf(getPicartoCode(res.locals.user)) > 0) {
            // get the correctly capitalized username
            // I think this should work even for multistreams, at least until Picarto decides to fuck things up again or do something useful for a change, whichever comes first.
            var username = body.match(/popoutStream\(\'(.+?)\',\'\',\'public\'\)/);
            username = (username && username.length > 1) ? username[1] : req.body.connect_picarto_username;
            // save picarto username to the user instance
            res.locals.user.picarto = username;
            return res.locals.user.save()
            .then(function () {
              return res.status(200).send({
                message: 'Succesfully connected the account! Remember to delete the code from the description.',
                username: username,
              });
            })
            .catch(function (err) {
              return res.status(500).send({
                message: err.message,
                user: res.locals.user.previous(),
              });
            });
          } else {
            return res.status(400).send({
              message: 'The code could not be found.'
            });
          }
        }
        return res.status(400).send({
          message: 'Something went wrong fetching the Picarto page. Please try again later.'
        });
      });
    // if disconnecting picarto from the account
    } else if (req.body.disconnect_picarto) {
      // remove and save
      res.locals.user.picarto = null;
      return res.locals.user.save()
      .then(function () {
        return res.status(200).send({
          message: 'Succesfully disconnected the account!',
        });
      })
      .catch(function (err) {
        return res.status(500).send({
          message: err.message,
          user: res.locals.user.previous(),
        });
      });
    // what are you here for if you're not doing anything
    } else {
      return res.status(400).send({
        message: 'Missing information.'
      });
    }
  });

  router.get('/admin/news', function(req, res) {
    if (req.query.id) {
      res.redirect(req.baseUrl+'/admin/news/'+req.query.id);
    } else {
      db.models.NewsArticle.findAll({
        order: 'createdAt DESC'
      })
      .then(function (articles) {
        res.render('settings-news', {
          articles: articles
        });
      });
    }
  });
  router.get('/admin/news/:id', function(req, res) {
    db.models.NewsArticle.findById(req.params.id)
    .then(function (article) {
      res.render('settings-news', {
        article: article
      });
    });
  });
  router.post('/admin/news/', bodyParser.urlencoded({extended:true}), function(req, res) {
    db.models.NewsArticle.upsertWithReturn({
      where: {id: {[Op.eq]:req.body.id}},
      defaults: req.body
    })
    .spread(function (article, created) {
      if (created) {
        req.flash('success', 'News article "'+article.header+'" posted.');
      } else {
        req.flash('success', 'News article "'+article.header+'" updated.');
      }
      res.redirect(req.baseUrl+'/admin/news/'+article.id);
    })
    .catch(function (err) {
      req.flash('danger', err.message);
      res.redirect(req.baseUrl+'/admin/news/');
    })
  });

  return router;
};