'use strict';
var bodyParser = require('body-parser');
var router = require('express').Router();
var Op = require('sequelize').Op;

module.exports = function (app, db) {

  // app.use(bodyParser.json());
  // app.use(bodyParser.urlencoded({ extended: true }));

  router.get('/', function(req, res) {
    res.render('auth', {
      bodyClasses: 'page-auth'
    });
  });

  router.get('/signup', function(req, res) {
    res.render('auth-signup', {
      bodyClasses: 'page-auth'
    });
  });

  router.post('/login', bodyParser.urlencoded({extended:true}), function(req, res) {
    if (!req.body.username || !req.body.password) {
      return res.status(400).send({
        message: 'All of the fields have to be filled.'
      });
    }
    db.models.User.findOne({
      where: db.whereNocase('name', req.body.username)
    })
    .then(function (user) {
      if (!user) {
        return res.status(401).send({
          message: 'Incorrect username or password.',
        });
      }
      user.verifyPassword(req.body.password, function (err, isMatch) {
        if (err) {
          return res.status(500).send({
            message: err,
          });
        }
        if (isMatch !== true) {
          return res.status(401).send({
            message: 'Incorrect username or password.',
          });
        }
        if (!user.active) {
          return res.status(401).send({
            message: 'Account has not been activated.',
          });
        }
        // logged in succesfully, set user to session
        req.session.userId = user.id;
        return res.status(200).send({
          // message: 'Logged in succesfully',
          redirect: '/',
        });
      });
    });
  });

  router.get('/logout', function(req, res) {
    req.session = null;
    res.redirect('/');
  });

  router.post('/signup', bodyParser.urlencoded({extended:true}), function(req, res) {
    if (!req.body.username || !req.body.password || !req.body.password_confirm || !req.body.email) {
      return res.status(400).send({
        message: 'All of the fields have to be filled.'
      });
    }

    db.models.User.findOrCreate({
      where: {
        name: {[Op.eq]:req.body.username},
        email: {[Op.eq]:req.body.email},
      },
      defaults: {
        password: req.body.password,
        password_confirm: req.body.password_confirm,
        // TODO: remove for email confirmation
        active: true,
      },
    })
    .catch(db.database.UniqueConstraintError, db.database.ValidationError, function (err) {
      // console.error(err);
      return res.status(403).send({
        // message: 'Username or email is already taken.',
        message: err.errors.map(el => el.message).join(';')
      });
    })
    .catch(function (err) {
      console.error(err);
      return res.status(500).send(err);
    })
    .spread(function (user, created) {
      if (!created) {
        return res.status(403).send({
          message: 'Username or email is already taken.',
        });
      } else {
        // TODO: email confirmation
        // log the user in right away
        req.session.userId = user.id;
        return res.status(200).send({
          // message: 'User created succesfully',
          redirect: '/',
        });
      }
    });
  });

  return router;
};