'use strict';
var router = require('express').Router();

module.exports = function(app, db, chat) {

  // index
  router.get('/', function(req, res) {
    res.render('circlejerk', {});
  });

  return router;
};